package com.example.inz.ViewFromFragmentsLesse;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.InsertView.InsertHouse;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.Breakdown;
import com.example.inz.ViewFromFragments.Message;
import com.example.inz.ViewFromFragments.Payment;
import com.example.inz.ViewFromFragments.Visits;
import com.example.inz.adapterRecacleView.HousesAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HouseAgreementView extends Fragment {
    SharedPreferences sharedPref;
    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    TextView txtHouseName, txtHouseM2, txtHouseAdress, txtAgrementName,txtDateFrom,txtDateTo,txtPayment,txtDateOfPayment,txtDeposit,txtPeriodOfNotice;
    Button btnMessages,btnBreakdown,btnVisits, btnPayment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewHouseAgreement = inflater.inflate(R.layout.house_agreement_lesse,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Agreement");
        txtHouseName = viewHouseAgreement.findViewById(R.id.txtHouseDetailNameLessee);
        txtHouseM2 = viewHouseAgreement.findViewById(R.id.txtHouseDetailM2Lessee);
        txtHouseAdress = viewHouseAgreement.findViewById(R.id.txtHouseDetailAddressLessee);
        txtAgrementName = viewHouseAgreement.findViewById(R.id.txtHouseDetailAgreementNameLessee);
        txtDateFrom = viewHouseAgreement.findViewById(R.id.txtHouseDetailDateFromLessee);
        txtDateTo = viewHouseAgreement.findViewById(R.id.txtHouseDetailDateToLessee);
        txtPayment = viewHouseAgreement.findViewById(R.id.txtHouseDetailPaymentLessee);
        txtDateOfPayment= viewHouseAgreement.findViewById(R.id.txtHouseDetailDateOfPaymentLessee);
        txtDeposit = viewHouseAgreement.findViewById(R.id.txtHouseDetailDepositLessee);
        txtPeriodOfNotice = viewHouseAgreement.findViewById(R.id.txtHouseDetailPeriodOfNoticeLessee);

        btnBreakdown = viewHouseAgreement.findViewById(R.id.btnBreakdownLesse);
        btnMessages = viewHouseAgreement.findViewById(R.id.btnMessageLessee);
        btnVisits = viewHouseAgreement.findViewById(R.id.btnVisitsLessee);
        btnPayment = viewHouseAgreement.findViewById(R.id.btnPaymentLessee);

        btnVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Visits()).addToBackStack(null).commit();
            }
        });

        btnMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Message()).addToBackStack(null).commit();
            }
        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Payment()).addToBackStack(null).commit();
            }
        });

        btnBreakdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Breakdown()).addToBackStack(null).commit();
            }
        });

        setDataHouses();
        return viewHouseAgreement;
    }



    private void  setDataHouses() {


        try {
            sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            String jsonData = sharedPref.getString(Markers.housesLesseeName, "error");
            HouseID houseID = gson.fromJson(jsonData, HouseID.class);

            String houseM2 = "Size: " + houseID.getHouseID().get(Markers.positionHouse).getSize()+ " m2";
            String dateFrom = "Date from: " + houseID.getHouseID().get(Markers.positionHouse).getDateFrom();
            String dateTo = "Date to: " + houseID.getHouseID().get(Markers.positionHouse).getDateTo();
            String monthlyPayment = "Payment: " +  houseID.getHouseID().get(Markers.positionHouse).getMonthlyPayment() + " $";
            String dayOfPayment = "Day Of Payment: " +  houseID.getHouseID().get(Markers.positionHouse).getDateDuePayment();
            String deposit = "Deposit: " + houseID.getHouseID().get(Markers.positionHouse).getDeposit() + " $";
            String periodOfNotice = "Period of notice: " + houseID.getHouseID().get(Markers.positionHouse).getDateDueTerminate();
            txtHouseName.setText(houseID.getHouseID().get(Markers.positionHouse).getHouseName());
            txtHouseM2.setText(houseM2);
            txtHouseAdress.setText(houseID.getHouseID().get(Markers.positionHouse).getAddress());
            txtAgrementName.setText(houseID.getHouseID().get(Markers.positionHouse).getAgrementName());
            txtDateFrom.setText(dateFrom);
            txtDateTo.setText(dateTo);
            txtPayment.setText(monthlyPayment);
            txtDateOfPayment.setText(dayOfPayment);
            txtDeposit.setText(deposit);
            txtPeriodOfNotice.setText(periodOfNotice);



        }
        catch (Exception e)

        {

        }






    }
}
