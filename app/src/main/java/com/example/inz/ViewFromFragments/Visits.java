package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.FurnitureID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.VisitsID;
import com.example.inz.InsertView.InsertFurniture;
import com.example.inz.InsertView.InsertVisit;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.FurnitureAdapter;
import com.example.inz.adapterRecacleView.VisitsAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Visits extends Fragment {
    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    RelativeLayout relativeLayoutVisits;
    TextView txtVisitsLoad;
    FloatingActionButton btnVisitsAdd;
    RecyclerView recyclerViewVisits;
    LinearLayoutManager linearLayoutManagerVisits;
    SharedPreferences sharedPref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewVisits = inflater.inflate(R.layout.visits,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Visits");
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);


        relativeLayoutVisits = viewVisits.findViewById(R.id.layVisits);

        txtVisitsLoad = viewVisits.findViewById(R.id.txtVisitsLoad);
        recyclerViewVisits= viewVisits.findViewById(R.id.recViewVisits);

        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled},
                new int[] { android.R.attr.state_pressed}
        };
        int[] colors = new int[] {
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blackerBlue)
        };
        ColorStateList fabColorList = new ColorStateList(states, colors);
        viewVisits.findViewById(R.id.floatingBtnAddVisits).setBackgroundTintList(fabColorList);


        linearLayoutManagerVisits = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewVisits.setLayoutManager(linearLayoutManagerVisits);
        btnVisitsAdd = viewVisits.findViewById(R.id.floatingBtnAddVisits);
        btnVisitsAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new InsertVisit()).addToBackStack(null).commit();
            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(getVisitsJson());

        return viewVisits;
    }


















    private JsonObjectRequest getVisitsJson() {
        String url = "https://aligator1.herokuapp.com/select/visitByHouse";
        String jsonData;
        if(Markers.ifLessorLessee==0) {
            jsonData = sharedPref.getString(Markers.housesName, "error");
        }
        else{
            jsonData = sharedPref.getString(Markers.housesLesseeName, "error");
            relativeLayoutVisits.removeView(btnVisitsAdd);
        }
        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        Map<String, String> params = new HashMap();
        params.put(Markers.houseID,houseIDs);
        JSONObject parameters = new JSONObject(params);



        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    VisitsID visitsID = gson.fromJson(jsonData.toString(),VisitsID.class);

                    Log.d("params", visitsID.getVisitsID().get(0).getVisitsName());

                    visitsID.getVisitsID().get(0).getDateFrom();
                    relativeLayoutVisits.removeView(txtVisitsLoad);
                    VisitsAdapter visitsAdapter = new VisitsAdapter(getActivity(),visitsID, getFragmentManager());
                    recyclerViewVisits.setAdapter(visitsAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();

                    txtVisitsLoad.setText("No Visits");
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                txtVisitsLoad.setText("No Visits");
            }
        });


        return jsonRequest;
    }
}
