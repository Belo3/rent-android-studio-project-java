package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.FurnitureID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.PaymentID;
import com.example.inz.InsertView.InsertFurniture;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.FurnitureAdapter;
import com.example.inz.adapterRecacleView.PaymentAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Furniture extends Fragment {
    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    RelativeLayout relativeLayoutFurniture;
    TextView txtFurnitureLoad;
    FloatingActionButton btnFurnitureAdd;
    RecyclerView recyclerViewFurniture;
    LinearLayoutManager linearLayoutManagerFurniture;
    SharedPreferences sharedPref;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFurniture = inflater.inflate(R.layout.furniture,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Furniture");
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        relativeLayoutFurniture = viewFurniture.findViewById(R.id.layFurniture);

        txtFurnitureLoad = viewFurniture.findViewById(R.id.txtFurnitureLoading);
        recyclerViewFurniture = viewFurniture.findViewById(R.id.recVewFurniture);

        linearLayoutManagerFurniture = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewFurniture.setLayoutManager(linearLayoutManagerFurniture);



        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled},
                new int[] { android.R.attr.state_pressed}
        };
        int[] colors = new int[] {
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blackerBlue)
        };
        ColorStateList fabColorList = new ColorStateList(states, colors);
        viewFurniture.findViewById(R.id.flotingBtnAddFurniture).setBackgroundTintList(fabColorList);
        btnFurnitureAdd = viewFurniture.findViewById(R.id.flotingBtnAddFurniture);
        btnFurnitureAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new InsertFurniture()).addToBackStack(null).commit();
            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(getFurnishingJson());


        return viewFurniture;
    }











    private JsonObjectRequest getFurnishingJson() {
        String url = "https://aligator1.herokuapp.com/select/furnishing";

        String jsonData = sharedPref.getString("houses","error");

        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        Map<String, String> params = new HashMap();
        params.put(Markers.houseID,houseIDs);
        JSONObject parameters = new JSONObject(params);



        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    FurnitureID furnitureID = gson.fromJson(jsonData.toString(),FurnitureID.class);

                    Log.d("params", furnitureID.getFurnitureID().get(0).getFurnishingName());

                    furnitureID.getFurnitureID().get(0).getFurnishingName();
                    relativeLayoutFurniture.removeView(txtFurnitureLoad);
                    FurnitureAdapter furnitureAdapter = new FurnitureAdapter(getActivity(),furnitureID, getFragmentManager(),recyclerViewFurniture);
                    recyclerViewFurniture.setAdapter(furnitureAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();

                    txtFurnitureLoad.setText("No Furniture");
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                txtFurnitureLoad.setText("No Furniture");
            }
        });


        return jsonRequest;
    }
}
