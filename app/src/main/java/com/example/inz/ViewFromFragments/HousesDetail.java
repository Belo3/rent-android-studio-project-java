package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.MySingleton;
import com.example.inz.InsertView.InsertAgreement;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.AgreementsAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HousesDetail extends Fragment {
    TextView txtHouseName, txtHouseM2,txtHouseAddress, txtIfNoAgre;
    Button btnFurniture, btnBreakdown, btnVisits;
    RecyclerView recyclerViewAgreements;
    SharedPreferences sharedPref;
    Gson gson = new Gson();
    HouseID houseID;
    int positionHouse;
    String houseJson;
    JsonObjectRequest jsonRequest;
    RelativeLayout layoutHousesDetail;
    AgreementsAdapter agreementsAdapter;
    AgreementID agreementID;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();
    String jsonDataAgreements;
    SharedPreferences.Editor editor;
    FloatingActionButton floatingActionButtonAgrement;


    private LinearLayoutManager mLinearLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View ViewHouseDetail = inflater.inflate(R.layout.house_detail,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("House");
        positionHouse = Markers.positionHouse;
        layoutHousesDetail = ViewHouseDetail.findViewById(R.id.layHousesDetail);

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        houseJson = sharedPref.getString("houses","lol");
        houseID = gson.fromJson(houseJson , HouseID.class);
        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled},
                new int[] { android.R.attr.state_pressed}
        };
        int[] colors = new int[] {
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blackerBlue)
        };
        ColorStateList fabColorList = new ColorStateList(states, colors);
        ViewHouseDetail.findViewById(R.id.flotingBtnAddAgreement).setBackgroundTintList(fabColorList);
        floatingActionButtonAgrement = ViewHouseDetail.findViewById(R.id.flotingBtnAddAgreement);


        txtHouseName = ViewHouseDetail.findViewById(R.id.txtHouseDetailNameLessee);
        txtHouseM2 = ViewHouseDetail.findViewById(R.id.txtHouseDetailM2Lessee);
        txtHouseAddress = ViewHouseDetail.findViewById(R.id.txtHouseDetailAddressLessee);
        txtIfNoAgre = ViewHouseDetail.findViewById(R.id.txtIfNoAgreements);
        recyclerViewAgreements = ViewHouseDetail.findViewById(R.id.recViewAgreements);
        btnFurniture = ViewHouseDetail.findViewById(R.id.btnFurniture);
        btnVisits = ViewHouseDetail.findViewById(R.id.btnVisits);
        btnBreakdown = ViewHouseDetail.findViewById(R.id.btnBreakdown);

        if(houseID.getHouseID().get(Markers.positionHouse).getNumberOfBreakdown() !=0 ){
            btnBreakdown.setText( houseID.getHouseID().get(Markers.positionHouse).getNumberOfBreakdown() + " Breakdown" );
            btnBreakdown.setTextColor(getContext().getColor(R.color.red));
        }

        txtHouseName.setText(houseID.getHouseID().get(positionHouse).getHouseName());
        txtHouseAddress.setText(houseID.getHouseID().get(positionHouse).getAddress());
        txtHouseM2.setText(houseID.getHouseID().get(positionHouse).getSize() + "m2");

        btnFurniture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Furniture()).addToBackStack(null).commit();
            }
        });

        btnVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Visits()).addToBackStack(null).commit();
            }
        });
        btnBreakdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Breakdown()).addToBackStack(null).commit();
            }
        });

        floatingActionButtonAgrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new InsertAgreement()).addToBackStack(null).commit();
            }
        });

        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerViewAgreements.setLayoutManager(mLinearLayoutManager);


        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.putString(Markers.agreementsName, null);
        editor.apply();
        MySingleton.getInstance(getContext()).addToRequestQueue(getHousesJson());

        return ViewHouseDetail;
    }





    private JsonObjectRequest getHousesJson() {
        String url = "https://aligator1.herokuapp.com/select/agreementByHouse";



        String jsonData = sharedPref.getString("houses","error");



        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(positionHouse).getIdHouse();

        Map<String, String> params = new HashMap();
        params.put("houseID",houseIDs);
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                agreementID= gson.fromJson(jsonData.toString() , AgreementID.class);
                editor.putString(Markers.agreementsName, jsonData.toString());
                editor.apply();
                try {
                    agreementID.getAgreementID().get(0).getHouseID();
                    layoutHousesDetail.removeView(txtIfNoAgre);
                    agreementsAdapter = new AgreementsAdapter(getActivity(),agreementID,getFragmentManager());
                    recyclerViewAgreements.setAdapter(agreementsAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();
                    txtIfNoAgre.setText("No Agreements :( ");
                    txtIfNoAgre.setTextSize(40);
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                txtIfNoAgre.setText("No Agreements :(");
            }
        });


        return jsonRequest;
    }
}
