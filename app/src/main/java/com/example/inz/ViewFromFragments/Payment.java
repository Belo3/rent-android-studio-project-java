package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.PaymentID;
import com.example.inz.InsertView.InsertPayment;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.HousesAdapter;
import com.example.inz.adapterRecacleView.PaymentAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



public class Payment extends Fragment {

    FloatingActionButton btnAdd;
    SharedPreferences sharedPref;
    Gson gson = new Gson();
    JsonObjectRequest jsonRequest;
    TextView txtPaymentLoad;
    RelativeLayout layPayment;
    LinearLayoutManager linearLayoutManagerPayment;
    RecyclerView recyclerViewPayment;
    PaymentAdapter paymentAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewPayment = inflater.inflate(R.layout.payment,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Payment");




        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        recyclerViewPayment = viewPayment.findViewById(R.id.recRecPayment);

        linearLayoutManagerPayment = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        btnAdd = viewPayment.findViewById(R.id.flotingBtnAddPayment);

        recyclerViewPayment.setLayoutManager(linearLayoutManagerPayment);
        layPayment = viewPayment.findViewById(R.id.layPayment);
        if(Markers.ifLessorLessee==0){
            int[][] states = new int[][] {
                    new int[] { android.R.attr.state_enabled},
                    new int[] { android.R.attr.state_pressed}
            };
            int[] colors = new int[] {
                    ContextCompat.getColor(getContext(), R.color.green),
                    ContextCompat.getColor(getContext(), R.color.blackerBlue)
            };
            ColorStateList fabColorList = new ColorStateList(states, colors);
            viewPayment.findViewById(R.id.flotingBtnAddPayment).setBackgroundTintList(fabColorList);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame,new InsertPayment()).addToBackStack(null).commit();
                }
            });
        }else{
            layPayment.removeView(btnAdd);
        }



        txtPaymentLoad = viewPayment.findViewById(R.id.txtPaymentLoad);


        MySingleton.getInstance(getContext()).addToRequestQueue(getPaymentJson());
        return viewPayment;
    }







    private JsonObjectRequest getPaymentJson() {
        String url = "https://aligator1.herokuapp.com/select/paymentByAgreement";


        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        String agreID;
        if(Markers.ifLessorLessee==0) {

            String jsonData = sharedPref.getString(Markers.agreementsName,"error");
            AgreementID agreementID = gson.fromJson(jsonData, AgreementID.class);
            agreID = agreementID.getAgreementID().get(Markers.positionAgreement).getAgreementID();
        }
        else{
            String jsonInputData = sharedPref.getString(Markers.housesLesseeName, "error");
            HouseID houseID = gson.fromJson(jsonInputData, HouseID.class);
            agreID = houseID.getHouseID().get(Markers.positionHouse).getAgrementID();

        }



        Map<String, String> params = new HashMap();
        params.put(Markers.agrementID,agreID);
        JSONObject parameters = new JSONObject(params);



        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {

                try {
                    PaymentID paymentID = gson.fromJson(jsonData.toString() ,PaymentID.class);
                    paymentID.getPaymentID().get(0).getPaymentName();
                    editor.putString("payment",jsonData.toString());
                    editor.apply();
                    layPayment.removeView(txtPaymentLoad);
                    paymentAdapter = new PaymentAdapter(getActivity(),paymentID, getFragmentManager(), recyclerViewPayment);
                    recyclerViewPayment.setAdapter(paymentAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();
                    txtPaymentLoad.setText("No Payments :( ");
                    txtPaymentLoad.setTextSize(40);
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                txtPaymentLoad.setText("No Payments :(");
            }
        });


        return jsonRequest;
    }
}
