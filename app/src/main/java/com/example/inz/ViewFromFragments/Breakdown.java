package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.BreakDownID;
import com.example.inz.Data.FurnitureID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.MySingleton;
import com.example.inz.InsertView.InsertBreakDown;
import com.example.inz.InsertView.InsertFurniture;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.BreakdownAdapter;
import com.example.inz.adapterRecacleView.FurnitureAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Breakdown extends Fragment {
    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    RelativeLayout relativeLayoutBreakdown;
    TextView txtBreakdownLoad;
    FloatingActionButton btnBreakdownAdd;
    RecyclerView recyclerViewBreakdown;
    LinearLayoutManager linearLayoutManagerBreakdown;
    SharedPreferences sharedPref;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View viewBreakdown = inflater.inflate(R.layout.breakdown,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Breakdown");
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        txtBreakdownLoad = viewBreakdown.findViewById(R.id.txtBreakdownLoad);
        btnBreakdownAdd = viewBreakdown.findViewById(R.id.floatingBtnAddBreakdown);
        relativeLayoutBreakdown = viewBreakdown.findViewById(R.id.layBreakdown);


            int[][] states = new int[][] {
                    new int[] { android.R.attr.state_enabled},
                    new int[] { android.R.attr.state_pressed}
            };
            int[] colors = new int[] {
                    ContextCompat.getColor(getContext(), R.color.blue),
                    ContextCompat.getColor(getContext(), R.color.black)
            };
            ColorStateList fabColorList = new ColorStateList(states, colors);
            viewBreakdown.findViewById(R.id.floatingBtnAddBreakdown).setBackgroundTintList(fabColorList);

            btnBreakdownAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new InsertBreakDown()).addToBackStack(null).commit();
                }
            });






        recyclerViewBreakdown = viewBreakdown.findViewById(R.id.recViewBreakdown);

        linearLayoutManagerBreakdown = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewBreakdown.setLayoutManager(linearLayoutManagerBreakdown);

        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAdapterBreakdown(getContext(), getFragmentManager(), relativeLayoutBreakdown, txtBreakdownLoad, recyclerViewBreakdown));

        return viewBreakdown;
    }









}
