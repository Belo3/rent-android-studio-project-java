package com.example.inz.ViewFromFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MessageID;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.MessageAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.sql.StatementEvent;

public class Message extends Fragment {
    Button btnSendMessage;
    SharedPreferences sharedPref;
    Gson gson = new Gson();
    SharedPreferences.Editor editor;
    RecyclerView recyclerViewMessage;
    RelativeLayout relativeLayoutMessage;
    LinearLayoutManager mLinearLayoutManager;
    EditText editTextMessage;
    private String agreIdString, userID;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewMessage = inflater.inflate(R.layout.message,container,false);
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        relativeLayoutMessage = viewMessage.findViewById(R.id.recLayMessages);
        editTextMessage = viewMessage.findViewById(R.id.txtMessage);

        recyclerViewMessage = viewMessage.findViewById(R.id.recMessage);

        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerViewMessage.setLayoutManager(mLinearLayoutManager);

        btnSendMessage = viewMessage.findViewById(R.id.btnSendMessage);


        MySingleton.getInstance(getContext()).addToRequestQueue(getMessageChat());

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mesageText = editTextMessage.getText().toString().trim();
                if(mesageText.isEmpty()){

                }else{

                    MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAddMessage(getContext(),getFragmentManager(),mesageText,agreIdString,userID,viewMessage,recyclerViewMessage));
                }


            }
        });



        return viewMessage;
    }















    private JsonObjectRequest getMessageChat() {
        String url = "https://aligator1.herokuapp.com/select/messageAgeement";

        if (Markers.ifLessorLessee==0) {
            String jsonInputData = sharedPref.getString(Markers.agreementsName, "error");
            AgreementID agreementID = gson.fromJson(jsonInputData, AgreementID.class);
            agreIdString = agreementID.getAgreementID().get(Markers.positionAgreement).getAgreementID();
            ((MainActivity)getActivity()).getSupportActionBar().setTitle(agreementID.getAgreementID().get(Markers.positionAgreement).getHouseOwnerName() + " " + agreementID.getAgreementID().get(Markers.positionAgreement).getHouseOwnerSurname());


        }else{
            String jsonInputData = sharedPref.getString(Markers.housesLesseeName, "error");
            HouseID houseID = gson.fromJson(jsonInputData, HouseID.class);
            agreIdString = houseID.getHouseID().get(Markers.positionHouse).getAgrementID();
            ((MainActivity)getActivity()).getSupportActionBar().setTitle(houseID.getHouseID().get(Markers.positionHouse).getOnwerName() + " " + houseID.getHouseID().get(Markers.positionHouse).getOwnerSurName());

        }


        String jsonData = sharedPref.getString(Markers.userName,"error");

        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        userID = loginid.getLoginData().get(0).getIdUser();

        Map<String, String> params = new HashMap();
        params.put("agreID",agreIdString);
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                MessageID messageID = gson.fromJson(jsonData.toString() , MessageID.class);
                try {


                    messageID.getMessageID().get(0).getMessageID();
                    MessageAdapter messageAdapter = new MessageAdapter(getActivity(),messageID,getFragmentManager(), Integer.parseInt( userID));
                    recyclerViewMessage.setAdapter(messageAdapter);
                    recyclerViewMessage.scrollToPosition(messageID.getMessageID().size()-1);

                }
                catch (Exception e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        return jsonRequest;
    }


}
