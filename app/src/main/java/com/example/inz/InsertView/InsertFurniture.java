package com.example.inz.InsertView;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;

public class InsertFurniture extends Fragment {
    TextView txtNameFur, txtSizeFur;
    Button btnAdd;
    FetchJsonClass fetchJsonClassHouse = new FetchJsonClass();
    JsonObjectRequest jsonObjectRequest;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewInsertHouse = inflater.inflate(R.layout.add_funiture,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Add Furniture");
        getFragmentManager();


        txtNameFur = viewInsertHouse.findViewById(R.id.editAddFurnitureName);
        txtSizeFur = viewInsertHouse.findViewById(R.id.editAddFurnitureSize);

        btnAdd = viewInsertHouse.findViewById(R.id.btnAddFurniture);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String furName = txtNameFur.getText().toString();
                String furSize = txtSizeFur.getText().toString().trim();

                if((furName.isEmpty()) || (furSize.isEmpty())){
                    CharSequence text = "Fill All";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }
                else{

                jsonObjectRequest = fetchJsonClassHouse.jsonCallUrlAddFurniture(getContext(),getFragmentManager(),furName,furSize,getView());

                MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

                    fetchJsonClassHouse.clearForm((ViewGroup) getView().findViewById(R.id.layAddFurniture));
                }
            }
        });



        return viewInsertHouse;
    }


}
