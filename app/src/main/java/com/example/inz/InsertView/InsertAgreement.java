package com.example.inz.InsertView;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;

import java.util.Calendar;

public class InsertAgreement extends Fragment {
    TextView txtDateTo, txtDateFrom;
    EditText editAgreName, editAgreNumberOfPeople, editAgrePrice, editAgreDateOFPayment, editAgreDeposit, editAgreNoticePeriod, editAgreEmail;
    String agreName, agreNumberOfPeople, agrePrice, agreDateOFPayment, agreDeposit, agreNoticePeriod, agreEmail, dateFrom, dateTo;
    Button btnAddAgreement;
    private DatePickerDialog.OnDateSetListener mDateSetListenerDateFrom, mDateSetListenerDateTo;
    FetchJsonClass fetchJsonClassHouse = new FetchJsonClass();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View viewInsertAgreement = inflater.inflate(R.layout.add_agreement,container,false);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Registration");

        editAgreName = viewInsertAgreement.findViewById(R.id.txtAddAgreementName);
        editAgreNumberOfPeople = viewInsertAgreement.findViewById(R.id.txtAddAgreementPopulace);
        editAgrePrice = viewInsertAgreement.findViewById(R.id.txtAddAgreementPayment);
        editAgreDateOFPayment = viewInsertAgreement.findViewById(R.id.txtAddAgreementDatePayed);
        editAgreDeposit = viewInsertAgreement.findViewById(R.id.txtAddAgreementDeposit);
        editAgreNoticePeriod = viewInsertAgreement.findViewById(R.id.txtAddAgreementPeriodOfNotice);
        editAgreEmail = viewInsertAgreement.findViewById(R.id.txtAddAgreementEmail);
        txtDateFrom = viewInsertAgreement.findViewById(R.id.txtAddAgreementDateFrom);
        txtDateTo = viewInsertAgreement.findViewById(R.id.txtAddAgreementDateTo);
        btnAddAgreement = viewInsertAgreement.findViewById(R.id.btnInsertAgreement);






//region             DATE PICKER
        txtDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog( getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListenerDateFrom, year,month,day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListenerDateFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                String date = year + "/" + month + "/"+ day;
                txtDateFrom.setText(date);
            }
        };


        txtDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog( getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListenerDateTo, year,month,day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListenerDateTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                String date = year + "/" + month + "/"+ day;
                txtDateTo.setText(date);
            }
        };

//endregion


        btnAddAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agreName = editAgreName.getText().toString();
                agreNumberOfPeople = editAgreNumberOfPeople.getText().toString();
                agrePrice = editAgrePrice.getText().toString();
                agreDateOFPayment = editAgreDateOFPayment.getText().toString();
                dateFrom = txtDateFrom.getText().toString();
                dateTo = txtDateTo.getText().toString();
                agreDeposit = editAgreDeposit.getText().toString();
                agreNoticePeriod = editAgreNoticePeriod.getText().toString();
                agreEmail = editAgreEmail.getText().toString();

                if((agreName.isEmpty()) || (agreNumberOfPeople.isEmpty())|| (agrePrice.isEmpty())|| (agreDateOFPayment.isEmpty())|| (dateFrom.isEmpty())||
                        (dateTo.isEmpty())|| (agreDeposit.isEmpty())|| (agreNoticePeriod.isEmpty())|| (agreEmail .isEmpty())){
                    CharSequence text = "Fill all boxes!";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }
                else {
                    JsonObjectRequest jsonObjectRequest = fetchJsonClassHouse.jsonCallUrlAddAgreement(getContext(), getFragmentManager(),
                            agreName,agreNumberOfPeople,dateFrom,dateTo,agrePrice,agreDateOFPayment,agreDeposit,agreNoticePeriod,agreEmail,getView());

                    MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        });







        return viewInsertAgreement;
    }





}
