package com.example.inz.InsertView;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;

public class InsertHouse extends Fragment {
    TextView txtName, txtSize, txtStreet, txtStreetNr,txtApartmentNr, txtCity, txtDescryption;
    Button btnAdd;
    FetchJsonClass fetchJsonClassHouse = new FetchJsonClass();
    JsonObjectRequest jsonObjectRequest;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewInsertHouse = inflater.inflate(R.layout.add_house,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Add House");
        getFragmentManager();
        txtName = viewInsertHouse.findViewById(R.id.txtAddHouseName);
        txtSize = viewInsertHouse.findViewById(R.id.txtAddHouseSize);
        txtStreet = viewInsertHouse.findViewById(R.id.txtAddHouseStreet);
        txtStreetNr = viewInsertHouse.findViewById(R.id.txtAddHouseStreetNr);
        txtApartmentNr = viewInsertHouse.findViewById(R.id.txtAddHouseNrNr);
        txtCity = viewInsertHouse.findViewById(R.id.txtAddHouseCity);
        txtDescryption = viewInsertHouse.findViewById(R.id.txtAddHouseDescription);

        btnAdd = viewInsertHouse.findViewById(R.id.btnAddHouse);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jsonObjectRequest = fetchJsonClassHouse.jsonCallUrlAddHouse(getContext(),getFragmentManager(),
                        txtName.getText().toString(),txtSize.getText().toString(),txtStreet.getText().toString(),
                        txtStreetNr.getText().toString(),txtApartmentNr.getText().toString(),txtCity.getText().toString(),
                        txtDescryption.getText().toString(),viewInsertHouse);

                MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
            }
        });



        return viewInsertHouse;
    }
}
