package com.example.inz.InsertView;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;

public class InsertBreakDown  extends Fragment {
    TextView txtNameBreak, txtDescriptBreak;
    Button btnAddBreak;
    FetchJsonClass fetchJsonClassHouse = new FetchJsonClass();
    JsonObjectRequest jsonObjectRequest;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewInsertBreakdown = inflater.inflate(R.layout.add_breakdown,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Add Breakdown");
        getFragmentManager();


        txtNameBreak = viewInsertBreakdown.findViewById(R.id.editAddBreakdownName);
        txtDescriptBreak = viewInsertBreakdown.findViewById(R.id.editAddBreakdownDescription);

        btnAddBreak = viewInsertBreakdown.findViewById(R.id.btnAddBreakdown);
        btnAddBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String breakName = txtNameBreak.getText().toString();
                String BreakSize = txtDescriptBreak.getText().toString().trim();

                if((breakName.isEmpty()) || (BreakSize.isEmpty())){
                    CharSequence text = "Fill All";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }
                else{

                    jsonObjectRequest = fetchJsonClassHouse.jsonCallUrlAddBreakdown(getContext(),getFragmentManager(),getView(),breakName,BreakSize);

                    MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        });



        return viewInsertBreakdown;
    }
}
