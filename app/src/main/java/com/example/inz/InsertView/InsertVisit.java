package com.example.inz.InsertView;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;

import java.util.Calendar;

public class InsertVisit extends Fragment {
    TextView txtVisitDate, txtVisitTime;
    EditText editVisitName, editVisitDescription;
    Button btnAddVisits;
    private DatePickerDialog.OnDateSetListener mDateSetListenerDateOfVisit;
    private TimePickerDialog.OnTimeSetListener mDateSetListenerTimeOfVisit;
    FetchJsonClass fetchJsonClassVisits = new FetchJsonClass();
    JsonObjectRequest jsonObjectRequest;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewInsertVisit = inflater.inflate(R.layout.add_visit,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Add Visit");
        txtVisitDate = viewInsertVisit.findViewById(R.id.txtAddVisitsDate);
        editVisitName = viewInsertVisit.findViewById(R.id.editAddVisitsName);
        editVisitDescription = viewInsertVisit.findViewById(R.id.editAddVisitsSentence);
        btnAddVisits = viewInsertVisit.findViewById(R.id.btnAddVisit);
        txtVisitTime = viewInsertVisit.findViewById(R.id.txtAddVisitsTime);

        //region setTime
        txtVisitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog( getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListenerDateOfVisit, year,month,day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListenerDateOfVisit = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                String date = year + "/" + month + "/"+ day;
                txtVisitDate.setText(date);
            }
        };

        txtVisitTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog( getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListenerTimeOfVisit, hour,minute,true);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListenerTimeOfVisit = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {

                String date = hour + ":" + minute ;
                txtVisitTime.setText(date);
            }
        };

//endregion


        btnAddVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String visName = editVisitName.getText().toString();
                String visDescryption = editVisitDescription.getText().toString().trim();
                String visTime = txtVisitTime.getText().toString().trim();
                String visDate= txtVisitDate.getText().toString().trim();

                if((visName.isEmpty()) || (visDescryption.isEmpty())|| (visTime.isEmpty())|| (visDate.isEmpty())){
                    CharSequence text = "Fill all boxes!";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }
                else {
                    String visCompleteTime = visDate+ " " + visTime;
                    jsonObjectRequest = fetchJsonClassVisits.jsonCallUrlAddVisit(getContext(), getFragmentManager(), getView(), visName, visDescryption, visCompleteTime);

                    MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        });

        return viewInsertVisit;
    }
}
