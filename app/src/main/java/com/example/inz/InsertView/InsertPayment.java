package com.example.inz.InsertView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.PaymentID;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.Payment;
import com.google.gson.Gson;

public class InsertPayment extends Fragment {
    EditText paymentName, paymentPrice;
    Button paymentAdd;
    FetchJsonClass fetchJsonAddPayment = new FetchJsonClass();
    JsonObjectRequest jsonObjectRequest;
    RelativeLayout relativeLayout;
    Gson gson = new Gson();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewInsertAgreement = inflater.inflate(R.layout.add_payment,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Add Payment");
        relativeLayout = viewInsertAgreement.findViewById(R.id.layAddPayment);

        paymentAdd = viewInsertAgreement.findViewById(R.id.btnAddPayment);
        paymentName = viewInsertAgreement.findViewById(R.id.editAddPaymentName);
        paymentPrice = viewInsertAgreement.findViewById(R.id.editAddPaymentPrice);

        if(Markers.ifLessorLessee == 0) {


            paymentAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String payName = paymentName.getText().toString().trim();
                    String payPrice = paymentPrice.getText().toString().trim();

                    if ((payName.isEmpty()) || (payPrice.isEmpty())) {
                        CharSequence text = "Fill All";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(getContext(), text, duration);
                        toast.show();
                    } else {

                        jsonObjectRequest = fetchJsonAddPayment.jsonCallUrlAddPayment(getContext(), getFragmentManager(), getView(), payName, payPrice);

                        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                    }
                }
            });
        }
        else {
            ((MainActivity)getActivity()).getSupportActionBar().setTitle("Pay");
            relativeLayout.removeView(paymentName);
            paymentAdd.setText("Pay");
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            String jsonInputData = sharedPref.getString("payment", "error");
            final PaymentID paymentID = gson.fromJson(jsonInputData, PaymentID.class);

            paymentAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!(paymentPrice.getText().toString().isEmpty())) {
                        Integer agreID = paymentID.getPaymentID().get(Markers.positionPayment).getPaymentPayed();
                        Integer price = Integer.parseInt(paymentPrice.getText().toString());
                        if (price > agreID) {
                            String priceS = Integer.toString(price);
                            String payID = Integer.toString(paymentID.getPaymentID().get(Markers.positionPayment).getPaymentID());
                            MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonAddPayment.jsonCallUrlPayPayment(getContext(), getFragmentManager(), payID, priceS));
                        }else{
                            CharSequence text = "Enter bigger price";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(getContext(), text, duration);
                            toast.show();
                        }

                    }else{
                        CharSequence text = "Fill All";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(getContext(), text, duration);
                        toast.show();
                    }
                }
            });



        }

        return viewInsertAgreement;
    }
}
