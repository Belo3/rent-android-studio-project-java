package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FurnitureID {

    @SerializedName("furnishing")
    private List<FurnitureData> FurnitureID;

    private FurnitureID(List<FurnitureData> FurnitureID) {
        this.FurnitureID = FurnitureID;
    }

    public List<FurnitureData> getFurnitureID() {
        return FurnitureID;
    }
}
