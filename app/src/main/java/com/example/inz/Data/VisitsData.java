package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitsData {

    @SerializedName("idWizyt")
        @Expose
        private String visitsID;
        @SerializedName("Nazwa")
        @Expose
        private String visitsName;
        @SerializedName("Tresc")
        @Expose
        private String visitsSentence;
        @SerializedName("dateVisit")
        @Expose
        private String dateVisit;
        @SerializedName("dateFrom")
        @Expose
        private String dateFrom;
        @SerializedName("houseID")
        @Expose
        private String houseID;



    public String getVisitsID() {
        return visitsID;
    }

    public String getVisitsName() {
        return visitsName;
    }

    public String getVisitsSentence() {
        return visitsSentence;
    }

    public String getDateVisit() {
        return dateVisit;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getHouseID() {
        return houseID;
    }
}
