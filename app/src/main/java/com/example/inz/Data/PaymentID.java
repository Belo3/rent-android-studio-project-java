package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentID {




    @SerializedName("payment")
    @Expose
    private List<PaymentData> PaymentID;

    public PaymentID(List<PaymentData> paymentID) {
        PaymentID = paymentID;
    }

    public List<PaymentData> getPaymentID() {
        return PaymentID;
    }
}
