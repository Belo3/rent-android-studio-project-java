package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifyDamageData {

    @SerializedName("notifyDamageID")
    @Expose
    private String notifyDamageID;
    @SerializedName("Opis")
    @Expose
    private String sentence;
    @SerializedName("Czas")
    @Expose
    private String timeAdded;
    @SerializedName("Data_Awarii")
    @Expose
    private String time;
    @SerializedName("damageID")
    @Expose
    private String damageID;

    public String getNotifyDamageID() {
        return notifyDamageID;
    }

    public String getSentence() {
        return sentence;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public String getDamageID() {
        return damageID;
    }

    public String getTime() {
        return time;
    }
}
