package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisitsID {

    @SerializedName("visits")
    private List<VisitsData> VisitsID;

    public List<VisitsData> getVisitsID() {
        return VisitsID;
    }
}
