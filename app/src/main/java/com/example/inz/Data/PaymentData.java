package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;

public class PaymentData {



    @SerializedName("idOplaty")
    @Expose
    private Integer paymentID;
    @SerializedName("Nazwa")
    @Expose
    private String paymentName;
    @SerializedName("Kwota")
    @Expose
    private Integer paymentPrice;
    @SerializedName("Data_Dodania")
    @Expose
    private String dateAdded;
    @SerializedName("Kwota_Zaplacona")
    @Expose
    private Integer paymentPayed;
    @SerializedName("Data_Zaplacenia")
    @Expose
    private String datePayed;
    @SerializedName("agreementID")
    @Expose
    private Integer agreementID;



    public Integer getPaymentID() {
        return paymentID;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public Integer getPaymentPrice() {
        return paymentPrice;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public Integer getPaymentPayed() {
        return paymentPayed;
    }

    public String getDatePayed() {
        return datePayed;
    }

    public Integer getAgreementID() {
        return agreementID;
    }



}
