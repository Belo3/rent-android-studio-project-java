package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

public class MessageData {
    @SerializedName("idWiadomosci")
    private String messageID;
    @SerializedName("Tresc")
    private String sentence;
    @SerializedName("Data_Wiadomosci")
    private String dateOfMessage;
    @SerializedName("Przeczytane")
    private Integer Read;
    @SerializedName("eventID")
    private String eventID;
    @SerializedName("Imie")
    private String Name;
    @SerializedName("idUzytkownicy")
    private Integer userID;
    @SerializedName("Nazwisko")
    private String surName;


    public String getMessageID() {
        return messageID;
    }

    public String getSentence() {
        return sentence;
    }

    public String getDateOfMessage() {
        return dateOfMessage;
    }

    public Integer getRead() {
        return Read;
    }

    public String getEventID() {
        return eventID;
    }

    public String getName() {
        return Name +" " +  surName;
    }

    public Integer getUserID() {
        return userID;
    }






}
