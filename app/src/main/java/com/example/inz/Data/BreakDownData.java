package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BreakDownData {

    @SerializedName("idAwarie")
    @Expose
    private String breakdownID;
    @SerializedName("Nazwa_Awarii")
    @Expose
    private String breakdownName;
    @SerializedName("Data_Awarii")
    @Expose
    private String dateAdded;
    @SerializedName("Opis_Awarii")
    @Expose
    private String breakdownSentence;
    @SerializedName("Akceptacja_Awarii")
    @Expose
    private Integer breakdownAccepted;
    @SerializedName("Rozwiazanie_Awarii")
    @Expose
    private Integer breakdownResolved;
    @SerializedName("houseID")
    @Expose
    private String houseID;
    @SerializedName("Email")
    @Expose
    private String email;

    public String getBreakdownID() {
        return breakdownID;
    }

    public String getBreakdownName() {
        return breakdownName;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public String getBreakdownSentence() {
        return breakdownSentence;
    }

    public Integer getBreakdownAccepted() {
        return breakdownAccepted;
    }

    public Integer getBreakdownResolved() {
        return breakdownResolved;
    }

    public String getHouseID() {
        return houseID;
    }

    public String getEmail() {
        return email;
    }


}
