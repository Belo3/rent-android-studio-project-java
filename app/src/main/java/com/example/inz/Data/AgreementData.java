package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgreementData {



    @SerializedName("Nazwa_Mieszkania")
    @Expose
    private String houseName;
    @SerializedName("Adres")
    @Expose
    private String houseAdress;



    @SerializedName("Imie")
    @Expose
    private String houseOwnerName;
    @SerializedName("Nazwisko")
    @Expose
    private String houseOwnerSurname;
    @SerializedName("ownerID")
    @Expose
    private String ownerID;
    @SerializedName("idUmowy")
    @Expose
    private String agreementID;
    @SerializedName("Nazwa")
    @Expose
    private String agreementName;
    @SerializedName("Data_Od")
    @Expose
    private String dateFrom;
    @SerializedName("Data_Do")
    @Expose
    private String dateTo;
    @SerializedName("Nr_Mieszkancow")
    @Expose
    private String nrOfResidents;
    @SerializedName("Kwota_Miesieczna")
    @Expose
    private String monthlyPayment;
    @SerializedName("Termin_Platnosci")
    @Expose
    private String dateDuePayment;
    @SerializedName("Kaucja")
    @Expose
    private String deposit;
    @SerializedName("Okres_Wypowiedzenia")
    @Expose
    private String dateDueTerminate;
    @SerializedName("Aktywna")
    @Expose
    private String active;
    @SerializedName("houseID")
    @Expose
    private String houseID;
    @SerializedName("wynajmujacyID")
    @Expose
    private String rentierID;
    @SerializedName("bilans")
    @Expose
    private Integer balance;



    @SerializedName("New Message Owner")
    @Expose
    private Integer newMessOwner;
    @SerializedName("New Message Rentier")
    @Expose
    private Integer newMessRentier;




    public String getAgreementID() {
        return agreementID;
    }

    public String getAgreementName() {
        return agreementName;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public String getNrOfResidents() {
        return nrOfResidents;
    }

    public String getMonthlyPayment() {
        return monthlyPayment;
    }

    public String getDateDuePayment() {
        return dateDuePayment;
    }

    public String getDeposit() {
        return deposit;
    }

    public String getDateDueTerminate() {
        return dateDueTerminate;
    }

    public String getActive() {
        return active;
    }

    public String getHouseID() {
        return houseID;
    }

    public String getRentierID() {
        return rentierID;
    }

    public Integer getBalance() {
        return balance;
    }
    public Integer getNewMessOwner() {
        return newMessOwner;
    }

    public Integer getNewMessRentier() {
        return newMessRentier;
    }
    public String getHouseName() {
        return houseName;
    }

    public String getHouseAdress() {
        return houseAdress;
    }
    public String getHouseOwnerName() {
        return houseOwnerName;
    }

    public String getHouseOwnerSurname() {
        return houseOwnerSurname;
    }
}
