package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageID {

    @SerializedName("message")
    @Expose
    private List<MessageData> MessageID;
    public List<MessageData> getMessageID() {
        return MessageID;
    }


}
