package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifyVisitsData {
    @SerializedName("notifyVisitID")
    @Expose
    private String notifyVisitsID;
    @SerializedName("Opis")
    @Expose
    private String sentence;
    @SerializedName("Czas")
    @Expose
    private String timeAdded;
    @SerializedName("visitID")
    @Expose
    private String visitsID;


    public String getNotifyVisitsID() {
        return notifyVisitsID;
    }

    public String getSentence() {
        return sentence;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public String getVisitsID() {
        return visitsID;
    }
}
