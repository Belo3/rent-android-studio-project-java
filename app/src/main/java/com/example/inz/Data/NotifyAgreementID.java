package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotifyAgreementID {


    @SerializedName("NotifyAgre")
    private List<AgreementData> NotifyID;
    @SerializedName("NotifyDamage")
    private List<NotifyDamageData> NotifyDamage;
    @SerializedName("NotifyVisit")
    private List<NotifyVisitsData> NotifyVisits;

    public List<AgreementData> getNotifyID() {
        return NotifyID;
    }

    public List<NotifyDamageData> getNotifyDamage() {
        return NotifyDamage;
    }

    public List<NotifyVisitsData> getNotifyVisits() {
        return NotifyVisits;
    }
}
