package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class HouseData {



    @SerializedName("idMieszkania")
    private String idHouse;
    @SerializedName("Nazwa")
    private String houseName;
    @SerializedName("Metraz")
    private String size;
    @SerializedName("Ulica")
    private String street;
    @SerializedName("Nr_Ulicy")
    private String streetNumber;
    @SerializedName("Nr_Mieszkania")
    private String apartmentNumber;
    @SerializedName("Miasto")
    private String city;
    @SerializedName("Opis")
    private String desciption;
    @SerializedName("ownerID")
    private String ownerID;
    @SerializedName("bilans")
    private Integer balance;
    @SerializedName("Notify")
    private String notify;
    @SerializedName("adres")
    private String address;
    @SerializedName("idUmowy")
    private String agrementID;
    @SerializedName("NazwaUmowy")
    private String agrementName;
    @SerializedName("liczbaAwarii")
    private Integer NumberOfBreakdown;



    @SerializedName("Data_Od")
    @Expose
    private String dateFrom;
    @SerializedName("Data_Do")
    @Expose
    private String dateTo;
    @SerializedName("Nr_Mieszkancow")
    @Expose
    private String nrOfResidents;
    @SerializedName("Kwota_Miesieczna")
    @Expose
    private String monthlyPayment;
    @SerializedName("Termin_Platnosci")
    @Expose
    private String dateDuePayment;
    @SerializedName("Kaucja")
    @Expose
    private String deposit;
    @SerializedName("Okres_Wypowiedzenia")
    @Expose
    private String dateDueTerminate;


    @SerializedName("numberOfBreakdowns")
    private  String numberOfBreakdowns;
    @SerializedName("numberOfVisits")
    private  String numberOfVisits;
    @SerializedName("newMessages")
    private  String numberOfMessages;
    @SerializedName("Imie")
    private  String onwerName;
    @SerializedName("Nazwisko")
    private  String ownerSurName;


    public String getIdHouse() {
        return idHouse;
    }
    public String getHouseName() {
        return houseName;
    }
    public String getSize() {
        return size;
    }
    public String getStreet() {
        return street;
    }
    public String getStreetNumber() {
        return streetNumber;
    }
    public String getApartmentNumber() {
        return apartmentNumber;
    }
    public String getCity() {
        return city;
    }
    public String getDesciption() {
        return desciption;
    }
    public String getOwnerID() {
        return ownerID;
    }
    public Integer getBalance() {
        return balance;
    }
    public String getNotify() {
        return notify;
    }
    public String getAddress() { return address;}
    public Integer getNumberOfBreakdown() {
        return NumberOfBreakdown;
    }
    public String getAgrementID() {
        return agrementID;
    }
    public String getAgrementName() {
        return agrementName;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public String getNrOfResidents() {
        return nrOfResidents;
    }

    public String getMonthlyPayment() {
        return monthlyPayment;
    }

    public String getDateDuePayment() {
        return dateDuePayment;
    }

    public String getDeposit() {
        return deposit;
    }

    public String getDateDueTerminate() {
        return dateDueTerminate;
    }

    public String getNumberOfBreakdowns() {
        return numberOfBreakdowns;
    }

    public String getNumberOfVisits() {
        return numberOfVisits;
    }

    public String getNumberOfMessages() {
        return numberOfMessages;
    }

    public String getOnwerName() {
        return onwerName;
    }

    public String getOwnerSurName() {
        return ownerSurName;
    }
}

