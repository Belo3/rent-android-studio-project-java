package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AgreementID {
    @SerializedName("agreement")
    private List<AgreementData> AgreementID;

    public List<AgreementData> getAgreementID() {
        return AgreementID;
    }


}
