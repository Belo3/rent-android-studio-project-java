package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BreakDownID {
    @SerializedName("damage")
    @Expose
    private List<BreakDownData> BreakDownID ;
    public List<BreakDownData> getBreakDownID () {
        return BreakDownID ;
    }
}
