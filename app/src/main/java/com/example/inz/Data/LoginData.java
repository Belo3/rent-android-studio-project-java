package com.example.inz.Data;

import com.google.gson.annotations.SerializedName;

public class LoginData {
    @SerializedName("idUzytkownicy")
    private String IdUser;
    @SerializedName("Email")
    private String Email;
    @SerializedName("Imie")
    private String Name;
    @SerializedName("Nazwisko")
    private String SurName;
    @SerializedName("Nr_telefonu")
    private String Number;
    @SerializedName("Wlasciciel")
    private Integer Owner;
    @SerializedName("Wynajmujacy")
    private Integer Rentier;



    public String getIdUser() {
        return IdUser;
    }
    public String getEmail() {
        return Email;
    }
    public String getName() { return Name; }
    public String getSurName() {
        return SurName;
    }
    public String getNumber() {
        return Number;
    }
    public Integer getOwner() {
        return Owner;
    }
    public Integer getRentier() {
        return Rentier;
    }



}
