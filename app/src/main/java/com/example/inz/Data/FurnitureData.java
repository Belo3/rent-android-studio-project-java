package com.example.inz.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FurnitureData {



    @SerializedName("idWyposażenie")
    @Expose
    private String furnishingID;
    @SerializedName("Nazwa")
    @Expose
    private String furnishingName;
    @SerializedName("Ilosc")
    @Expose
    private String furnishingSize;

    @SerializedName("houseID")
    @Expose
    private Object houseID;



    public FurnitureData(String urnishingID, String furnishingName, String numberOfFur, Object houseID) {
        this.furnishingID = urnishingID;
        this.furnishingName = furnishingName;
        this.furnishingSize = numberOfFur;
        this.houseID = houseID;
    }

    public String getFurnishingID() {
        return furnishingID;
    }

    public String getFurnishingName() {
        return furnishingName;
    }

    public String getFurnishingSize() {
        return furnishingSize;
    }

    public Object getHouseID() {
        return houseID;
    }


}
