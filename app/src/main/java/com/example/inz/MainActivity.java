package com.example.inz;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.LoginID;
import com.example.inz.Fragments.Houses_Fragment;
import com.example.inz.Fragments.LessorNotification;
import com.example.inz.Fragments.Login;
import com.example.inz.Fragments.Notification;
import com.example.inz.Fragments.Registration;
import com.example.inz.Fragments.RentierHousesFragment;
import com.example.inz.Fragments.UserInfo;
import com.google.gson.Gson;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    Gson gson = new Gson();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);






        toolbar.setTitle("Login");
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Login()).commit();
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logOut) {
            Activity activity = (Activity) this;
            // or 'getSupportFragmentManager();'
            int count = getSupportFragmentManager().getBackStackEntryCount();
            for(int i = 0; i < count; ++i) {
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear().commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new Login()).commit();
            toolbar.setTitle("Login");
        }

        if (id == R.id.action_user_settings) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new UserInfo()).addToBackStack(null).commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Login) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new Login()).commit();
        }
        else if (id == R.id.nav_Houses) {
            try{

                Activity activity =  this;
                SharedPreferences sharedPref;
                sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                String jsonData = sharedPref.getString(Markers.userName,"error");

                LoginID loginid = gson.fromJson(jsonData , LoginID.class);
                if(loginid.getLoginData().get(0).getOwner() == 1){
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new Houses_Fragment()).commit();

                }else{
                    String text2 = "You are not Owner";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getApplicationContext(), text2, duration);
                    toast.show();
                }
            }
            catch (Exception i){

            }

        }

        else if (id == R.id.nav_Registration) {

            Markers.ifRegestierUserInfo = 0;
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new Registration()).commit();

        }

         else if (id == R.id.nav_Notfi) {

            try{

                Activity activity =  this;
                SharedPreferences sharedPref;
                sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                String jsonData = sharedPref.getString(Markers.userName,"error");

                LoginID loginid = gson.fromJson(jsonData , LoginID.class);
                if(loginid.getLoginData().get(0).getOwner() == 1){
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new LessorNotification()).commit();

                }else{
                    String text2 = "You are not Owner";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getApplicationContext(), text2, duration);
                    toast.show();
                }
            }
            catch (Exception i){

            }




        }

        else if (id == R.id.nav_Houses_Lessee) {
            try{

                Activity activity =  this;
                SharedPreferences sharedPref;
                sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                String jsonData = sharedPref.getString(Markers.userName,"error");

                LoginID loginid = gson.fromJson(jsonData , LoginID.class);
                if(loginid.getLoginData().get(0).getRentier() == 1){

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new RentierHousesFragment()).commit();

                }
                    else{
                        String text2 = "You are not Lessee";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(getApplicationContext(), text2, duration);
                        toast.show();
                    }

            }
            catch (Exception i){

            }
        } else if (id == R.id.nav_Notfi_Lesse) {
            Activity activity =  this;
            SharedPreferences sharedPref;
            sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
            String jsonData = sharedPref.getString(Markers.userName,"error");

            LoginID loginid = gson.fromJson(jsonData , LoginID.class);
            if(loginid.getLoginData().get(0).getRentier() == 1){

                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,new Notification()).commit();

            }else{
                String text2 = "You are not Lessee";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getApplicationContext(), text2, duration);
                toast.show();
            }



        }




        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
