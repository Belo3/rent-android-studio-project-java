package com.example.inz.ControlerVariable;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.BreakDownID;
import com.example.inz.Data.FurnitureID;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MessageID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.NotifyAgreementID;
import com.example.inz.Fragments.Login;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.BreakdownAdapter;
import com.example.inz.adapterRecacleView.FurnitureAdapter;
import com.example.inz.adapterRecacleView.MessageAdapter;
import com.example.inz.adapterRecacleView.NotifyAgreementAdapter;
import com.example.inz.adapterRecacleView.NotifyDamageAdapter;
import com.example.inz.adapterRecacleView.NotifyDamageLessorAdapter;
import com.example.inz.adapterRecacleView.NotifyVisitsAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class FetchJsonClass {
    Gson gson = new Gson();
    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText)view).setText("");
            }

            if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0))
                clearForm((ViewGroup)view);
        }
    }



    public JsonObjectRequest jsonCallUrlAddHouse
            (final Context context, final FragmentManager fragmentManager, String HouseName, String HouseSize, String HouseStreet,
             String HouseStreetNumber, String HouseApartmentNumber, String HouseCity, String HouseDescription, final View view)

    {

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();
        String url = "https://aligator1.herokuapp.com/insert/house";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",HouseName);
        params.put("metraz",HouseSize);
        params.put("ulica",HouseStreet);
        params.put("nrUlicy",HouseStreetNumber);
        params.put("nrMieszkania",HouseApartmentNumber);
        params.put("miasto",HouseCity);
        params.put("opis",HouseDescription);

        params.put("userID",userID);

        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
                hideKeyboardFrom(context,view);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlAddAgreement
            (final Context context, final FragmentManager fragmentManager, String agreName, String agreNumbOfPeople, String agreDateFrom,
             String agreDateTo, String agrePrice, String agreDayOfPayment, String agreDeposit, String agrenoticePeriod, String agreEmail, final View view)
    {

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.housesName,"error");
        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String sHouseID = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();
        String url = "https://aligator1.herokuapp.com/insert/agreement";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",agreName);
        params.put("dataOd",agreDateFrom);
        params.put("dataDo",agreDateTo);
        params.put("nrMieszkancow",agreNumbOfPeople);
        params.put("kwotaMiesieczna",agrePrice);
        params.put("terminPlatnosci",agreDayOfPayment);
        params.put("kaucja",agreDeposit);
        params.put("okresWypowiedzenia",agrenoticePeriod);
        params.put(Markers.houseID,sHouseID);

        params.put("Email",agreEmail);

        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
                hideKeyboardFrom(context,view);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAddFurniture
            (final Context context, final FragmentManager fragmentManager, String furName, String furSize, final View view)
    {

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.housesName,"error");
        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String sHouseID = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();
        String url = "https://aligator1.herokuapp.com/insert/furnishing";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",furName);
        params.put("ilosc",furSize);

        params.put(Markers.houseID,sHouseID);

        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {

                CharSequence text = "Added";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlAddVisit
            (final Context context, final FragmentManager fragmentManager, final View view , String visName, String visSentence, String visDateOfVisit )
    {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String todayDate = year + "/" + month + "/" + day;

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.housesName,"error");
        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String sHouseID = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();
        String url = "https://aligator1.herokuapp.com/insert/visit";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",visName);
        params.put("tresc",visSentence);
        params.put("dataWizytacji",visDateOfVisit);
        params.put("dataDodania",todayDate);
        params.put("aktywna","1");


        params.put(Markers.houseID,sHouseID);


        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");
                CharSequence text = "Added";
                int duration = Toast.LENGTH_SHORT;

                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
                hideKeyboardFrom(context,view);

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Params","BIG ERROR");
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAddPayment
            (final Context context, final FragmentManager fragmentManager, final View view , String payName, String payCost )
    {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String todayDate = year + "/" + month + "/" + day;

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.agreementsName,"error");
        AgreementID agreementID = gson.fromJson(jsonData , AgreementID.class);
        String sHouseID = agreementID.getAgreementID().get(Markers.positionAgreement).getAgreementID();
        String url = "https://aligator1.herokuapp.com/insert/payment";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",payName);
        params.put("kwota",payCost);
        params.put("dataDodania",todayDate);

        params.put("agreementID",sHouseID);


        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");
                CharSequence text = "Added";
                int duration = Toast.LENGTH_SHORT;

                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();

                hideKeyboardFrom(context,view);

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Params","BIG ERROR");
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAddBreakdown
            (final Context context, final FragmentManager fragmentManager, final View view , String breakName, String breakDescription )
    {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String todayDate = year + "/" + month + "/" + day;

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        String userData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(userData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();
        String houseIdS;
        String sHouseID;
        if(Markers.ifLessorLessee == 0) {
            houseIdS = sharedPref.getString(Markers.housesName, "error");
            HouseID houseID = gson.fromJson(houseIdS, HouseID.class);
            sHouseID = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();
        }else{
            String jsonInputData = sharedPref.getString(Markers.housesLesseeName, "error");
            HouseID houseID = gson.fromJson(jsonInputData, HouseID.class);
            sHouseID = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        }
        String url = "https://aligator1.herokuapp.com/insert/damage";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("nazwa",breakName);
        params.put("opisAwarii",breakDescription);
        params.put("dataAwarii",todayDate);
        if(Markers.ifLessorLessee == 0) {
            params.put("akceptacjaAwarii", "1");
        }else{
            params.put("akceptacjaAwarii", "0");
        }

        params.put("userID",userID);
        params.put(Markers.houseID,sHouseID);

        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");
                CharSequence text = "Added";
                int duration = Toast.LENGTH_SHORT;

                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();

                hideKeyboardFrom(context,view);

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Params","BIG ERROR");
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAcceptBreakdown
            (final Context context, final FragmentManager fragmentManager, String breakdownID, final Integer Mode, final RecyclerView recyclerView)
    {


        String url = "https://aligator1.herokuapp.com/update/damage";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("choice",Integer.toString(Mode));

        params.put("damageID",breakdownID);

        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");
                CharSequence text;
                try {
                    if (Mode == 0) {
                        text = "Accepted";
                        MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlASelectBreakdown(context, fragmentManager,recyclerView));


                    } else {
                        text = "Resolved";
                        MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlASelectBreakdown(context, fragmentManager, recyclerView));

                    }
                    int duration = Toast.LENGTH_SHORT;







                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                catch (Exception e)
                {

                    CharSequence text2 = "No More Breakdowns";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text2, duration);
                    toast.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Params","BIG ERROR");
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAdapterBreakdown(final Context context, final FragmentManager fragmentManager,
                                                         final RelativeLayout relativeLayout, final TextView textView, final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/damageSelectByhouseID";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        if(Markers.ifLessorLessee == 0) {
            jsonData = sharedPref.getString("houses", "error");

        }else{
            jsonData = sharedPref.getString(Markers.housesLesseeName, "error");
        }


        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put(Markers.houseID,houseIDs);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    BreakDownID breakDownID = gson.fromJson(jsonData.toString(),BreakDownID.class);
                    editor.putString(Markers.breakdownName, jsonData.toString());
                    editor.apply();
                    Log.d("params", breakDownID.getBreakDownID().get(0).getBreakdownName());


                    relativeLayout.removeView(textView);
                    BreakdownAdapter breakdownAdapter = new BreakdownAdapter(activity,breakDownID, fragmentManager, recyclerView);
                    recyclerView.setAdapter(breakdownAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();

                    textView.setText("No Breakdowns :)");
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                textView.setText("Error");
            }
        });


        return jsonRequest;
    }




    public JsonObjectRequest jsonCallUrlASelectBreakdown(final Context context, final FragmentManager fragmentManager,final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/damageSelectByhouseID";
        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString("houses","error");
        final SharedPreferences.Editor editor = sharedPref.edit();



        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        Map<String, String> params = new HashMap();
        params.put(Markers.houseID,houseIDs);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    BreakDownID breakDownID = gson.fromJson(jsonData.toString(),BreakDownID.class);
                    editor.putString(Markers.breakdownName, jsonData.toString());
                    editor.apply();
                    Log.d("params", jsonData.toString());
                    breakDownID.getBreakDownID().size();


                    BreakdownAdapter breakdownAdapter = new BreakdownAdapter(activity,breakDownID, fragmentManager, recyclerView);
                    recyclerView.swapAdapter(breakdownAdapter,true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlDeleteFurniture
            (final Context context, final FragmentManager fragmentManager, String furnitureID,  final RecyclerView recyclerView)
    {


        String url = "https://aligator1.herokuapp.com/delete/furnishing";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();

        params.put("furID",furnitureID);

        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");
                CharSequence text;
                try {

                    text = "Deleted";
                    MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlASelectFurniture(context, fragmentManager,recyclerView));

                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                catch (Exception e)
                {

                    CharSequence text2 = "No More Breakdowns";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text2, duration);
                    toast.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Params","BIG ERROR");
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }






    public JsonObjectRequest jsonCallUrlASelectFurniture(final Context context, final FragmentManager fragmentManager,final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/furnishing";
        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString("houses","error");
        final SharedPreferences.Editor editor = sharedPref.edit();



        HouseID houseID = gson.fromJson(jsonData , HouseID.class);
        String houseIDs = houseID.getHouseID().get(Markers.positionHouse).getIdHouse();

        Map<String, String> params = new HashMap();
        params.put(Markers.houseID,houseIDs);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    FurnitureID furnitureID = gson.fromJson(jsonData.toString(),FurnitureID.class);
                    editor.putString(Markers.breakdownName, jsonData.toString());
                    editor.apply();
                    Log.d("params", jsonData.toString());
                    furnitureID.getFurnitureID().size();


                    FurnitureAdapter breakdownAdapter = new FurnitureAdapter(activity,furnitureID, fragmentManager, recyclerView);
                    recyclerView.swapAdapter(breakdownAdapter,true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    fragmentManager.beginTransaction();
                    fragmentManager.popBackStack();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
            }
        });


        return jsonRequest;
    }




    public JsonObjectRequest jsonCallUrlRegister
            (final Context context, final FragmentManager fragmentManager, String regisEmail, String regisPassword, String regisName,
             String regisSurName, String regisNumber,boolean lessor, boolean lessie , final View view)
    {

        final Activity activity = (Activity) context;
        String url = "https://aligator1.herokuapp.com/insert/user";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("email",regisEmail);
        params.put("haslo",regisPassword);
        params.put("imie",regisName);
        params.put("nazwisko",regisSurName);
        params.put("numer",regisNumber);
        if(lessor) {
            params.put("wlasciciel", "1");
        }
        else{
            params.put("wlasciciel", "0");
        }
        if(lessie) {
        params.put("wynajmujacy", "1");
        }else{
            params.put("wynajmujacy", "0");
        }

        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                hideKeyboardFrom(context,view);

                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Login()).commit();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, "Registered!", duration);
                toast.show();




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }




    public JsonObjectRequest jsonCallUrlAddMessage
            (final Context context, final FragmentManager fragmentManager, String messageText, final String agreID, final String userID, final View view, final RecyclerView recyclerView)
    {

        final Activity activity = (Activity) context;
        String url = "https://aligator1.herokuapp.com/insert/message";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("tresc",messageText);
        params.put("agreID",agreID);

        params.put("userID",userID);
        Log.i("params", params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                MySingleton.getInstance(context).addToRequestQueue(getMessageChat(context,fragmentManager,agreID,userID,recyclerView, view));

                CharSequence text = "Sent";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "ErrorAddMessage";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }



    private JsonObjectRequest getMessageChat(final Context context, final FragmentManager fragmentManager, String agreIdString, final String userID, final RecyclerView recyclerView, final View view) {
        String url = "https://aligator1.herokuapp.com/select/messageAgeement";
        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);


        Map<String, String> params = new HashMap();
        params.put("agreID",agreIdString);
        params.put("userID",userID);
        Log.i("User id, event ID", params.toString());
        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                MessageID messageID = gson.fromJson(jsonData.toString() , MessageID.class);
                try {
                    messageID.getMessageID().get(0).getMessageID();
                    MessageAdapter messageAdapter = new MessageAdapter(activity,messageID,fragmentManager, Integer.parseInt(userID));
                    recyclerView.swapAdapter(messageAdapter, true);
                    recyclerView.scrollToPosition(messageID.getMessageID().size()-1);
                    clearForm((ViewGroup) view);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        return jsonRequest;
    }



    public JsonObjectRequest getHousesJson(final Context context, final FragmentManager fragmentManager) {
        String url = "https://aligator1.herokuapp.com/select/house";

        final Activity activity = (Activity) context;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        JsonObjectRequest jsonRequest;
        String jsonData = sharedPref.getString(Markers.userName,"error");
        final SharedPreferences.Editor editor = sharedPref.edit();


        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                HouseID houseID = gson.fromJson(jsonData.toString() , HouseID.class);

                editor.putString("houses", jsonData.toString());
                editor.apply();


                }
                catch (Exception e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlPayPayment
            (final Context context, final FragmentManager fragmentManager, String paymentID, String Price)
    {


        String url = "https://aligator1.herokuapp.com/update/paymentPaying";

        JsonObjectRequest jsonRequest;

        Map<String, String> params = new HashMap();
        params.put("kwota",Price);

        params.put("paymentID",paymentID);

        Log.i("Params",params.toString());
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                Log.i("Params","error");

                try {
                    CharSequence text = "payed";
                    fragmentManager.popBackStack();

                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                catch (Exception e)
                {

                    CharSequence text2 = "ERROR";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text2, duration);
                    toast.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlAdapterNotifyAgreement(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {


                    NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                    notifyAgreementID.getNotifyID().size();
                    Log.i("data", Integer.toString(notifyAgreementID.getNotifyID().size() ));
                    editor.putString(Markers.userNotifyLesseeAgreement, jsonData.toString());
                    editor.apply();
                    NotifyAgreementAdapter notifyAgreementAdapter = new NotifyAgreementAdapter(activity,notifyAgreementID, fragmentManager, recyclerView);
                    recyclerView.setAdapter(notifyAgreementAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();

                    Log.i("data", "error On try");
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Log.i("data", "on fetch");
            }
        });


        return jsonRequest;
    }





    public JsonObjectRequest jsonCallUrlAdapterSwapNotifyAgreement(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {


                    NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                    notifyAgreementID.getNotifyID().size();
                    Log.i("data", Integer.toString(notifyAgreementID.getNotifyID().size() ));
                    editor.putString(Markers.userNotifyLesseeAgreement, jsonData.toString());
                    editor.apply();
                    NotifyAgreementAdapter notifyAgreementAdapter = new NotifyAgreementAdapter(activity,notifyAgreementID, fragmentManager, recyclerView);
                    recyclerView.swapAdapter(notifyAgreementAdapter,true);


                }
                catch (Exception e) {
                    e.printStackTrace();

                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });


        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlUpdateAgreement(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView, int positon, final String choice) {
        String url = "https://aligator1.herokuapp.com/update/agreementActivation";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userNotifyLesseeAgreement,"error");
        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData , NotifyAgreementID.class);
        String agreementID = notifyAgreementID.getNotifyID().get(positon).getAgreementID();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();

        params.put("aktywna",choice);
        params.put("agreementID",agreementID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    String text;
                    if (choice.equals(0)){
                        text = "Added Agreement!";
                    }else{
                        text = "Declined Agreement!";
                    }

                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context,text , duration);
                    toast.show();
                    MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlAdapterSwapNotifyAgreement(context,fragmentManager,recyclerView));

                }
                catch (Exception e) {
                    e.printStackTrace();

                    Log.i("data", "error On try");
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Log.i("data", "on fetch");
            }
        });


        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAdapterNotifyDamage(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = jsonCallUrlAdapterSwapNotifyDamage(context,fragmentManager,recyclerView,0);


        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlAdapterSwapNotifyDamage(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView, final Integer choice) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {

                    if(choice==0) {
                        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                        notifyAgreementID.getNotifyDamage().size();
                        editor.putString(Markers.userNotifyLesseeDamage, jsonData.toString());
                        editor.apply();
                        NotifyDamageAdapter notifyDamageAdapter = new NotifyDamageAdapter(activity,notifyAgreementID, fragmentManager, recyclerView);
                        recyclerView.setAdapter(notifyDamageAdapter);
                    }
                    else{
                        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                        notifyAgreementID.getNotifyDamage().size();
                        editor.putString(Markers.userNotifyLesseeDamage, jsonData.toString());
                        editor.apply();
                        NotifyDamageAdapter notifyDamageAdapter = new NotifyDamageAdapter(activity,notifyAgreementID, fragmentManager, recyclerView);
                        recyclerView.swapAdapter(notifyDamageAdapter,true);
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();

                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });


        return jsonRequest;
    }



    public JsonObjectRequest jsonCallUrlAdapterNotifyVisits(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);


        return jsonCallUrlAdapterSwapNotifyVisits(context,fragmentManager,recyclerView,0);
    }


    public JsonObjectRequest jsonCallUrlAdapterSwapNotifyVisits(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView, final Integer choice) {
        String url = "https://aligator1.herokuapp.com/select/notificationsForRent";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {

                    if(choice==0) {
                        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                        notifyAgreementID.getNotifyVisits().size();
                        editor.putString(Markers.userNotifyLesseeDamage, jsonData.toString());
                        editor.apply();
                        NotifyVisitsAdapter notifyVisitsAdapter = new NotifyVisitsAdapter(activity, notifyAgreementID, fragmentManager, recyclerView);
                        recyclerView.setAdapter(notifyVisitsAdapter);
                    }
                    else{
                        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                        notifyAgreementID.getNotifyVisits().size();
                        editor.putString(Markers.userNotifyLesseeDamage, jsonData.toString());
                        editor.apply();
                        NotifyVisitsAdapter notifyVisitsAdapter = new NotifyVisitsAdapter(activity, notifyAgreementID, fragmentManager, recyclerView);
                        recyclerView.swapAdapter(notifyVisitsAdapter,true);
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();

                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });


        return jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlUpdateNotifiVisitsDamage(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView, int positon, final Integer choice) {
        String url = "https://aligator1.herokuapp.com/update/notifyRead";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String iD;
        final SharedPreferences.Editor editor = sharedPref.edit();

        jsonData = sharedPref.getString(Markers.userNotifyLesseeDamage, "error");
        Map<String, String> params = new HashMap();
        if(choice == 0) {
            NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData, NotifyAgreementID.class);
            iD = notifyAgreementID.getNotifyVisits().get(positon).getNotifyVisitsID();
            params.put("choice", Integer.toString(choice));
            params.put("notifyID", iD);

            Log.i("params",params.toString());
        }

        else {
            NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData, NotifyAgreementID.class);
            iD = notifyAgreementID.getNotifyDamage().get(positon).getNotifyDamageID();
            params.put("choice", Integer.toString(choice));
            params.put("notifyID", iD);
            Log.i("params",params.toString());

        }

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                    if(choice==0){
                        MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlAdapterSwapNotifyVisits(context, fragmentManager, recyclerView,1));
                    }else{
                        MySingleton.getInstance(context).addToRequestQueue(jsonCallUrlAdapterSwapNotifyDamage(context, fragmentManager, recyclerView,1));
                    }




                } catch (Exception e) {
                    e.printStackTrace();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        return  jsonRequest;
    }


    public JsonObjectRequest jsonCallUrlUpdateUserInfo
            (final Context context, final FragmentManager fragmentManager, String regisName,
             String regisSurName, String regisNumber, final View view)
    {

        final Activity activity = (Activity) context;
        String url = "https://aligator1.herokuapp.com/update/userInfo";
        final SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        JsonObjectRequest jsonRequest;

        String jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        Map<String, String> params = new HashMap();
        params.put("imie",regisName);
        params.put("nazwisko",regisSurName);
        params.put("numer",regisNumber);
        params.put("userID",userID);

        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {


                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(Markers.userName, jsonData.toString());
                editor.apply();
                hideKeyboardFrom(context,view);
                fragmentManager.popBackStack();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, "Changed Info!", duration);
                toast.show();




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                CharSequence text = "Error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        return jsonRequest;
    }




    public JsonObjectRequest jsonCallUrlAdapterDamageOwner(final Context context, final FragmentManager fragmentManager, final RecyclerView recyclerView) {

        Log.i("damage", "Start");
        String url = "https://aligator1.herokuapp.com/select/notificationsForOwner";
        final Activity activity = (Activity) context;
        String jsonData;
        SharedPreferences sharedPref;
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        jsonData = sharedPref.getString(Markers.userName,"error");
        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        Log.i("damage", "Im here");
        final SharedPreferences.Editor editor = sharedPref.edit();
        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);



        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                try {
                        NotifyAgreementID notifyAgreementID = gson.fromJson(jsonData.toString(), NotifyAgreementID.class);
                        notifyAgreementID.getNotifyDamage().size();
                        editor.putString(Markers.userNotifyLessorDamage, jsonData.toString());
                        editor.apply();
                        NotifyDamageLessorAdapter notifyDamageAdapter = new NotifyDamageLessorAdapter(activity,notifyAgreementID, fragmentManager, recyclerView);
                        recyclerView.setAdapter(notifyDamageAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();

                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });


        return jsonRequest;
    }
}

