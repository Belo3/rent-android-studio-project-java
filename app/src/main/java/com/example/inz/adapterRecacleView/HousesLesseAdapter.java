package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.HousesDetail;
import com.example.inz.ViewFromFragmentsLesse.HouseAgreementView;

public class HousesLesseAdapter extends RecyclerView.Adapter<HousesLesseAdapter.HousesViewHolder> {

        private HouseID housedata;
        private Context mContext;
        private android.support.v4.app.FragmentManager fragmentManager;


        public HousesLesseAdapter(Context mContext, HouseID housedata, FragmentManager fragmentManager) {
            this.housedata = housedata;
            this.mContext = mContext;
            this.fragmentManager = fragmentManager;
        }

        @Override
        public HousesLesseAdapter.HousesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_house_agreement_lesse, parent, false);
            return new HousesLesseAdapter.HousesViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final HousesLesseAdapter.HousesViewHolder holder, final int position) {
            holder.circle.setText(housedata.getHouseID().get(position).getHouseName().substring(0, 1));
            holder.houseName.setText(housedata.getHouseID().get(position).getHouseName());
            holder.houseBalance.setText(housedata.getHouseID().get(position).getBalance() + "$");
            holder.houseNotify.setText("Notification " + housedata.getHouseID().get(position).getNotify());
            holder.agreementName.setText(housedata.getHouseID().get(position).getAgrementName());

            if (housedata.getHouseID().get(position).getBalance() < 0) {
                holder.houseBalance.setTextColor(ContextCompat.getColor(mContext, R.color.red));

            }


            holder.layHouseItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Markers.positionHouse = position;
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new HouseAgreementView()).addToBackStack(null).commit();
                }
            });

        }


        @Override
        public int getItemCount() {
            return housedata.getHouseID().size();
        }


        class HousesViewHolder extends RecyclerView.ViewHolder {

            TextView circle, agreementName;
            TextView houseName;
            TextView houseBalance;
            TextView houseNotify;
            RelativeLayout layHouseItem;

            HousesViewHolder(View itemView) {
                super(itemView);

                circle = itemView.findViewById(R.id.recShapeLesse);
                houseName = itemView.findViewById(R.id.recHouseNameLesse);
                houseBalance = itemView.findViewById(R.id.recHouseBalanceLesse);
                houseNotify = itemView.findViewById(R.id.recNotifyLesse);
                layHouseItem = itemView.findViewById(R.id.recHouseLessee);
                agreementName = itemView.findViewById(R.id.recHouseAgreLesse);
            }


        }

    }
