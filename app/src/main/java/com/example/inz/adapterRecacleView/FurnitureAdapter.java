package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.FurnitureID;
import com.example.inz.Data.MySingleton;
import com.example.inz.R;

public class FurnitureAdapter extends RecyclerView.Adapter<FurnitureAdapter.FurnitureViewHolder>{


    FetchJsonClass fetchJsonClass = new FetchJsonClass();



    private FurnitureID furnitureID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    private RecyclerView recyclerView;


    public FurnitureAdapter(Context mContext, FurnitureID furnitureID, FragmentManager fragmentManager,RecyclerView recyclerView) {
        this.furnitureID = furnitureID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public FurnitureAdapter.FurnitureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_furniture, parent, false);
        return new FurnitureAdapter.FurnitureViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final FurnitureAdapter.FurnitureViewHolder holder, final int position) {


        holder.furnitureName.setText(furnitureID.getFurnitureID().get(position).getFurnishingName());
        holder.furnitureSize.setText(furnitureID.getFurnitureID().get(position).getFurnishingSize());
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlDeleteFurniture(mContext,fragmentManager,furnitureID.getFurnitureID().get(position).getFurnishingID(),recyclerView));
            }
        });

    }


    @Override
    public int getItemCount() {
        return furnitureID.getFurnitureID().size();
    }


    class FurnitureViewHolder extends RecyclerView.ViewHolder {

        TextView furnitureName, furnitureSize;
        Button btnDelete;

        RelativeLayout layFurnitureItem;

        FurnitureViewHolder(View itemView) {
            super(itemView);
            furnitureName = itemView.findViewById(R.id.recTxtFurnitureName);
            furnitureSize = itemView.findViewById(R.id.recTxtFurnitureSize);
            btnDelete = itemView.findViewById(R.id.recBtnFurnitureDelete);
            layFurnitureItem = itemView.findViewById(R.id.recLayFurniture);
        }


    }
}
