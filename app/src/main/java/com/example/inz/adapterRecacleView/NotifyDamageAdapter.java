package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.NotifyAgreementID;
import com.example.inz.Data.VisitsID;
import com.example.inz.R;

public class NotifyDamageAdapter extends RecyclerView.Adapter<NotifyDamageAdapter.NotifyDamageHolder>  {


        private FetchJsonClass fetchJsonClass = new FetchJsonClass();

        private NotifyAgreementID notifyAgreementID;
        private Context mContext;
        private android.support.v4.app.FragmentManager fragmentManager;
        private RecyclerView recyclerView;


    public NotifyDamageAdapter(Context mContext, NotifyAgreementID notifyAgreementID, FragmentManager fragmentManager, RecyclerView recyclerView) {
        this.notifyAgreementID = notifyAgreementID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

        @Override
        public NotifyDamageAdapter.NotifyDamageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_damage_notify, parent, false);
        return new NotifyDamageAdapter.NotifyDamageHolder (view);
    }


        @Override
        public void onBindViewHolder(final NotifyDamageAdapter.NotifyDamageHolder holder, final int position) {



        holder.damageSentence.setText(notifyAgreementID.getNotifyDamage().get(position).getSentence());
        holder.damageDate.setText(notifyAgreementID.getNotifyDamage().get(position).getTimeAdded());

        holder.layDamageNotifyItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        holder.btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlUpdateNotifiVisitsDamage(mContext,fragmentManager,recyclerView,position,1));
            }
        });

    }


        @Override
        public int getItemCount() {
        return notifyAgreementID.getNotifyDamage().size();
    }


        class NotifyDamageHolder extends RecyclerView.ViewHolder {

            TextView damageSentence, damageDate;
            Button btnRead;

            RelativeLayout layDamageNotifyItem;

            NotifyDamageHolder(View itemView) {
                super(itemView);
                damageSentence = itemView.findViewById(R.id.recTxtDamageNotifyName);
                damageDate = itemView.findViewById(R.id.recTxtDamageNotifyDate);
                btnRead = itemView.findViewById(R.id.btnReadNotify);
                layDamageNotifyItem = itemView.findViewById(R.id.recViewNotifyDamageItem);
            }


        }
}
