package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.NotifyAgreementID;
import com.example.inz.Data.VisitsID;
import com.example.inz.R;

public class NotifyVisitsAdapter extends RecyclerView.Adapter<NotifyVisitsAdapter.NotifyVisitsHolder>  {



    private FetchJsonClass fetchJsonClass = new FetchJsonClass();
    private NotifyAgreementID notifyAgreementID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    private RecyclerView recyclerView;


    public NotifyVisitsAdapter(Context mContext, NotifyAgreementID notifyAgreementID, FragmentManager fragmentManager, RecyclerView recyclerView) {
        this.notifyAgreementID = notifyAgreementID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public NotifyVisitsAdapter.NotifyVisitsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_visits_notify, parent, false);
        return new NotifyVisitsAdapter.NotifyVisitsHolder (view);
    }


    @Override
    public void onBindViewHolder(final NotifyVisitsAdapter.NotifyVisitsHolder holder, final int position) {



        holder.visitsSentence.setText(notifyAgreementID.getNotifyVisits().get(position).getSentence());
        holder.visitsDate.setText(notifyAgreementID.getNotifyVisits().get(position).getTimeAdded());

        holder.layVisitsNotifyItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        holder.btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlUpdateNotifiVisitsDamage(mContext,fragmentManager,recyclerView,position,0));
            }
        });

    }


    @Override
    public int getItemCount() {
        return notifyAgreementID.getNotifyVisits().size();
    }


    class NotifyVisitsHolder extends RecyclerView.ViewHolder {

        TextView visitsSentence, visitsDate;
        Button btnRead;

        RelativeLayout layVisitsNotifyItem;

        NotifyVisitsHolder(View itemView) {
            super(itemView);
            visitsSentence = itemView.findViewById(R.id.recTxtVisitsNotifyName);
            visitsDate = itemView.findViewById(R.id.recTxtVisitsNotifyDate);
            btnRead = itemView.findViewById(R.id.btnReadNotify);
            layVisitsNotifyItem = itemView.findViewById(R.id.recViewNotifyVisitsItem);
        }


    }
}
