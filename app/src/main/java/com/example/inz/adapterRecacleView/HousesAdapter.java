package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.Data.HouseID;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.HousesDetail;


public class HousesAdapter extends RecyclerView.Adapter<HousesAdapter.HousesViewHolder> {

    private HouseID housedata;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;


    public HousesAdapter(Context mContext, HouseID housedata, FragmentManager fragmentManager) {
        this.housedata = housedata;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public HousesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_house, parent, false);
        return new HousesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final HousesViewHolder holder, final int position) {
        holder.circle.setText(housedata.getHouseID().get(position).getHouseName().substring(0, 1));
        holder.houseName.setText(housedata.getHouseID().get(position).getHouseName());
        holder.houseBalance.setText(housedata.getHouseID().get(position).getBalance() + "$");
        holder.houseNotify.setText("Notification " + housedata.getHouseID().get(position).getNotify());

        if (housedata.getHouseID().get(position).getBalance() < 0) {
            holder.houseBalance.setTextColor(ContextCompat.getColor(mContext, R.color.red));

        }


        holder.layHouseItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Markers.positionHouse = position;
                fragmentManager.beginTransaction().replace(R.id.content_frame, new HousesDetail()).addToBackStack(null).commit();
            }
        });

    }


    @Override
    public int getItemCount() {
        return housedata.getHouseID().size();
    }


    class HousesViewHolder extends RecyclerView.ViewHolder {

        TextView circle;
        TextView houseName;
        TextView houseBalance;
        TextView houseNotify;
        RelativeLayout layHouseItem;

        HousesViewHolder(View itemView) {
            super(itemView);

            circle = itemView.findViewById(R.id.recShape);
            houseName = itemView.findViewById(R.id.recHouseName);
            houseBalance = itemView.findViewById(R.id.recHouseBalance);
            houseNotify = itemView.findViewById(R.id.recNotify);
            layHouseItem = itemView.findViewById(R.id.recHouseItem);
        }


    }
}