package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.NotifyAgreementID;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.Message;
import com.example.inz.ViewFromFragments.Payment;

public class NotifyAgreementAdapter extends RecyclerView.Adapter<NotifyAgreementAdapter.NotifyViewsHolder> {
    private FetchJsonClass fetchJsonClass = new FetchJsonClass();



    private NotifyAgreementID notifyAgreementID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    RecyclerView recyclerView;


    public NotifyAgreementAdapter(Context mContext, NotifyAgreementID notifyAgreementID, FragmentManager fragmentManager, RecyclerView recyclerView) {
        this.notifyAgreementID = notifyAgreementID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public NotifyAgreementAdapter.NotifyViewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_agreements_notify, parent, false);
        return new NotifyAgreementAdapter.NotifyViewsHolder(view);
    }

    @Override
    public int getItemCount() {
        return notifyAgreementID.getNotifyID().size();
    }

    @Override
    public void onBindViewHolder(final NotifyAgreementAdapter.NotifyViewsHolder holder, final int position) {

        String agreName = notifyAgreementID.getNotifyID().get(position).getAgreementName();
        String houseName = notifyAgreementID.getNotifyID().get(position).getHouseName();
        String houseAddress = notifyAgreementID.getNotifyID().get(position).getHouseAdress();
        String agreFrom = "Date from: "+ notifyAgreementID.getNotifyID().get(position).getDateFrom();
        String agreTo ="Date to: "+ notifyAgreementID.getNotifyID().get(position).getDateTo();
        String agrePayment ="Payment: "+ notifyAgreementID.getNotifyID().get(position).getMonthlyPayment()+ " $";
        String agreDateOfPayment = "Day of payment: "+ notifyAgreementID.getNotifyID().get(position).getDateDuePayment();
        String agreDeposit = "Deposit: " + notifyAgreementID.getNotifyID().get(position).getDeposit()+ " $";
        String agrePeriodOfNotice = "Period of notice: " + notifyAgreementID.getNotifyID().get(position).getDateDuePayment();
        String ownerName = notifyAgreementID.getNotifyID().get(position).getHouseOwnerName() +" "+ notifyAgreementID.getNotifyID().get(position).getHouseOwnerSurname();






        holder.txtAgreementName.setText(agreName);
        holder.txtHouseName.setText(houseName);
        holder.txtHouseAddress.setText(houseAddress);
        holder.txtDateFrom.setText(agreFrom);
        holder.txtDateTo.setText(agreTo);
        holder.txtPayment.setText(agrePayment);
        holder.txtDateOfPayment.setText(agreDateOfPayment);
        holder.txtDeposit.setText(agreDeposit);
        holder.txtPeriodOfNotice.setText(agrePeriodOfNotice);
        holder.txtOwnerName.setText(ownerName);

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlUpdateAgreement(mContext,fragmentManager,recyclerView,position,"1"));
            }
        });

        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlUpdateAgreement(mContext,fragmentManager,recyclerView,position,"0"));
            }
        });


    }



























    class NotifyViewsHolder extends RecyclerView.ViewHolder {

        TextView txtHouseName, txtHouseAddress, txtAgreementName,txtDateFrom,txtDateTo,txtPayment,txtDateOfPayment,txtDeposit,txtPeriodOfNotice, txtOwnerName;
        Button accept, decline;

        RelativeLayout layNotifyAgreementItem;

        NotifyViewsHolder(View itemView) {
            super(itemView);
            txtHouseName = itemView.findViewById(R.id.recTxtHouseNameNotificationLessee);
            txtHouseAddress = itemView.findViewById(R.id.recTxtHouseNotificationAddressLessee);
            txtAgreementName = itemView.findViewById(R.id.recTxtHouseNotificationAgreementNameLessee);
            txtDateFrom = itemView.findViewById(R.id.recTxtHouseDetailDateFromLessee);
            txtDateTo = itemView.findViewById(R.id.recTxtHouseDetailDateToLessee);
            txtPayment = itemView.findViewById(R.id.recTxtHouseDetailPaymentLessee);
            txtDateOfPayment= itemView.findViewById(R.id.recTxtHouseDetailDateOfPaymentLessee);
            txtDeposit = itemView.findViewById(R.id.recTxtHouseDetailDepositLessee);
            txtPeriodOfNotice = itemView.findViewById(R.id.recTxtHouseDetailPeriodOfNoticeLessee);
            accept = itemView.findViewById(R.id.recBtnAcceptAgreement);
            decline = itemView.findViewById(R.id.recBtnDeclineAgreement);
            txtOwnerName = itemView.findViewById(R.id.recTxtHouseDetailHouseOwnerNameLessee);
            layNotifyAgreementItem = itemView.findViewById(R.id.recLayNotifyAgreement);

        }


    }
}
