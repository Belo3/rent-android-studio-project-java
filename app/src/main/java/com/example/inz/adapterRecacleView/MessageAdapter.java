package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.Data.MessageID;
import com.example.inz.R;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private MessageID messageID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    private Integer userID;


    public MessageAdapter(Context mContext, MessageID messageID, FragmentManager fragmentManager, Integer userID) {
        this.messageID = messageID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.userID = userID;
    }

    @Override
    public int getItemViewType(int position) {
        if(!(messageID.getMessageID().get(position).getUserID() == userID)) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType != 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
            return new MessageAdapter.MessagesViewHolder (view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_recived, parent, false);
            return new MessageAdapter.MessagesLeftViewHolder (view);
        }

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if(!(messageID.getMessageID().get(position).getUserID() == userID)) {
            populateSentViewHolder(holder, position);
        } else {
            populateReceivedViewHolder(holder, position);
        }

    }


    @Override
    public int getItemCount() {
        return messageID.getMessageID().size();
    }


    class MessagesViewHolder extends RecyclerView.ViewHolder {

        TextView messageName, messageRead, messageTime;

        RelativeLayout layMessageItem;

        MessagesViewHolder(View itemView) {
            super(itemView);
            messageName = itemView.findViewById(R.id.recTxtMessageText);
            messageRead = itemView.findViewById(R.id.recTxtMessageRead);
            messageTime = itemView.findViewById(R.id.recTxtMessageTime);
            layMessageItem = itemView.findViewById(R.id.recLayMessages);
        }


    }


    class MessagesLeftViewHolder extends RecyclerView.ViewHolder {

        TextView messageName, messageRead, messageTime;

        RelativeLayout layMessageItem;

        MessagesLeftViewHolder(View itemView) {
            super(itemView);
            messageName = itemView.findViewById(R.id.recTxtMessageLeftText);
            messageRead = itemView.findViewById(R.id.recTxtMessageLeftRead);
            messageTime = itemView.findViewById(R.id.recTxtMessageLeftTime);
            layMessageItem = itemView.findViewById(R.id.recLayMessagesLeft);
        }


    }


    private void populateSentViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(messageID.getMessageID().get(position).getRead() == 1){
            ((MessagesLeftViewHolder)holder).messageRead.setText("Read");
        }
        else{
            ((MessagesLeftViewHolder)holder).messageRead.setText("Not read");
        }
        ((MessagesLeftViewHolder)holder).messageName.setText(messageID.getMessageID().get(position).getSentence());
        ((MessagesLeftViewHolder)holder).messageTime.setText(messageID.getMessageID().get(position).getDateOfMessage());
    }

    private void populateReceivedViewHolder(RecyclerView.ViewHolder holder, int   position) {
        if(messageID.getMessageID().get(position).getRead() == 1){
            ((MessagesViewHolder)holder).messageRead.setText("Read");
        }
        else{
            ((MessagesViewHolder)holder).messageRead.setText("Not read");
        }
        ((MessagesViewHolder)holder).messageName.setText(messageID.getMessageID().get(position).getSentence());
        ((MessagesViewHolder)holder).messageTime.setText(messageID.getMessageID().get(position).getDateOfMessage());
    }


}
