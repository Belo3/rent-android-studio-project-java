package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.NotifyAgreementID;
import com.example.inz.R;

public class NotifyDamageLessorAdapter extends RecyclerView.Adapter<NotifyDamageLessorAdapter.NotifyDamageHolder>  {



    private NotifyAgreementID notifyAgreementID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    private RecyclerView recyclerView;


    public NotifyDamageLessorAdapter(Context mContext, NotifyAgreementID notifyAgreementID, FragmentManager fragmentManager, RecyclerView recyclerView) {
        this.notifyAgreementID = notifyAgreementID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public NotifyDamageLessorAdapter.NotifyDamageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_damage_lesser_notifi, parent, false);
        return new NotifyDamageLessorAdapter.NotifyDamageHolder (view);
    }


    @Override
    public void onBindViewHolder(final NotifyDamageLessorAdapter.NotifyDamageHolder holder, final int position) {



        holder.damageSentence.setText(notifyAgreementID.getNotifyDamage().get(position).getSentence());
        holder.damageDate.setText(notifyAgreementID.getNotifyDamage().get(position).getTime());

        holder.layDamageNotifyItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });



    }


    @Override
    public int getItemCount() {
        return notifyAgreementID.getNotifyDamage().size();
    }


    class NotifyDamageHolder extends RecyclerView.ViewHolder {

        TextView damageSentence, damageDate;

        RelativeLayout layDamageNotifyItem;

        NotifyDamageHolder(View itemView) {
            super(itemView);
            damageSentence = itemView.findViewById(R.id.recTxtDamageNotifyName);
            damageDate = itemView.findViewById(R.id.recTxtDamageNotifyDate);
            layDamageNotifyItem = itemView.findViewById(R.id.recNotifyDamageLessorItem);
        }


    }
}

