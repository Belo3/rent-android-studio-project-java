package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.PaymentID;
import com.example.inz.InsertView.InsertPayment;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.Message;
import com.example.inz.ViewFromFragments.Payment;

public class PaymentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{



    private PaymentID paymentID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;

    private RecyclerView recyclerView;


    public PaymentAdapter(Context mContext, PaymentID paymentID, FragmentManager fragmentManager, RecyclerView recyclerView) {
        this.paymentID = paymentID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemViewType(int position) {
        if(Markers.ifLessorLessee==0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType != 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment, parent, false);
            return new PaymentAdapter.PaymentsViewHolder(view);
        }
        else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_lessee, parent, false);
            return new PaymentAdapter.PaymentsViewHolderLessee(view);
        }
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if(Markers.ifLessorLessee == 0 ) {
            populatePaymentViewHolder(holder, position);
        } else {

            populatePaymentViewHolderLessee(holder, position);
        }

    }


    @Override
    public int getItemCount() {
        return paymentID.getPaymentID().size();
    }


    class PaymentsViewHolder extends RecyclerView.ViewHolder {

        TextView paymentName, paymentPrice, paymentDatePrice, paymentPayed, paymentDatePayed;

        RelativeLayout layPaymentItem;

        PaymentsViewHolder(View itemView) {
            super(itemView);
            paymentName = itemView.findViewById(R.id.recTxtPaymentName);
            paymentPrice = itemView.findViewById(R.id.recTxtPaymentPrice);
            paymentDatePrice = itemView.findViewById(R.id.recTxtPaymentDatePrice);
            paymentPayed = itemView.findViewById(R.id.recTxtPaymentPayed);
            paymentDatePayed = itemView.findViewById(R.id.recTxtPaymentDatePay);
            layPaymentItem = itemView.findViewById(R.id.recLayPayment);
        }


    }

    class PaymentsViewHolderLessee extends RecyclerView.ViewHolder {

        TextView paymentName, paymentPrice, paymentDatePrice,  paymentDatePayed,paymentPayed;
        RelativeLayout layPaymentItem;
        Button btnPay;

        PaymentsViewHolderLessee(View itemView) {
            super(itemView);
            paymentName = itemView.findViewById(R.id.recTxtPaymentNameLessee);
            paymentPrice = itemView.findViewById(R.id.recTxtPaymentPriceLessee);
            paymentDatePrice = itemView.findViewById(R.id.recTxtPaymentDatePriceLessee);
            paymentPayed = itemView.findViewById(R.id.recTxtPaymentPayedLessee);
            paymentDatePayed = itemView.findViewById(R.id.recTxtPaymentDatePayLessee);
            layPaymentItem = itemView.findViewById(R.id.recPaymentLesseeItem);
            btnPay = itemView.findViewById(R.id.btnPayPaymentLesseeLessee);
        }


    }


    private void populatePaymentViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((PaymentsViewHolder) holder).paymentName.setText(paymentID.getPaymentID().get(position).getPaymentName());

        ((PaymentsViewHolder) holder).paymentPrice.setText(Integer.toString( paymentID.getPaymentID().get(position).getPaymentPrice()) + " $");
        ((PaymentsViewHolder) holder).paymentDatePrice.setText(paymentID.getPaymentID().get(position).getDateAdded());
        if (paymentID.getPaymentID().get(position).getPaymentPayed() <= paymentID.getPaymentID().get(position).getPaymentPrice()){
            ((PaymentsViewHolder) holder).paymentPayed.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        }
        ((PaymentsViewHolder) holder).paymentPayed.setText(Integer.toString( paymentID.getPaymentID().get(position).getPaymentPayed()) + " $");

        if (paymentID.getPaymentID().get(position).getDatePayed()==null){
            ((PaymentsViewHolder) holder).paymentDatePayed.setText("");
        }
        else {
            ((PaymentsViewHolder) holder).paymentDatePayed.setText(paymentID.getPaymentID().get(position).getDatePayed());
        }
    }


    private void populatePaymentViewHolderLessee(final RecyclerView.ViewHolder holder,final int position) {

        ((PaymentsViewHolderLessee) holder).paymentName.setText(paymentID.getPaymentID().get(position).getPaymentName());

        ((PaymentsViewHolderLessee) holder).paymentPrice.setText(Integer.toString( paymentID.getPaymentID().get(position).getPaymentPrice()) + " $");
        ((PaymentsViewHolderLessee) holder).paymentDatePrice.setText(paymentID.getPaymentID().get(position).getDateAdded());
        if (paymentID.getPaymentID().get(position).getPaymentPayed() <= paymentID.getPaymentID().get(position).getPaymentPrice()){
            ((PaymentsViewHolderLessee) holder).paymentPayed.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        }
        ((PaymentsViewHolderLessee) holder).paymentPayed.setText(Integer.toString( paymentID.getPaymentID().get(position).getPaymentPayed()));

        if (paymentID.getPaymentID().get(position).getDatePayed()==null){
            ((PaymentsViewHolderLessee) holder).paymentDatePayed.setText("");
        }
        else {
            ((PaymentsViewHolderLessee) holder).paymentDatePayed.setText(paymentID.getPaymentID().get(position).getDatePayed());
        }
        ((PaymentsViewHolderLessee) holder).btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Markers.positionPayment = position;
                fragmentManager.beginTransaction().replace(R.id.content_frame, new InsertPayment()).addToBackStack(null).commit();


            }
        });

    }

}
