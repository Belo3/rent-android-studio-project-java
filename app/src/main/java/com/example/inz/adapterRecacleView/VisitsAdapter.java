package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.Data.MessageID;
import com.example.inz.Data.VisitsID;
import com.example.inz.R;

public class VisitsAdapter extends RecyclerView.Adapter<VisitsAdapter.VisitsViewHolder> {



    private VisitsID visitsID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;


    public VisitsAdapter(Context mContext, VisitsID visitsID, FragmentManager fragmentManager) {
        this.visitsID = visitsID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public VisitsAdapter.VisitsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visits, parent, false);
        return new VisitsAdapter.VisitsViewHolder  (view);
    }


    @Override
    public void onBindViewHolder(final VisitsAdapter.VisitsViewHolder holder, final int position) {



        holder.visitsName.setText(visitsID.getVisitsID().get(position).getVisitsName());
        holder.visitsDate.setText(visitsID.getVisitsID().get(position).getDateVisit());
        holder.visitsSentence.setText(visitsID.getVisitsID().get(position).getVisitsSentence());

        holder.layVisitsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

    }


    @Override
    public int getItemCount() {
        return visitsID.getVisitsID().size();
    }


    class VisitsViewHolder extends RecyclerView.ViewHolder {

        TextView visitsName, visitsDate, visitsSentence;

        RelativeLayout layVisitsItem;

        VisitsViewHolder(View itemView) {
            super(itemView);
            visitsName = itemView.findViewById(R.id.recTxtVisitsName);
            visitsDate = itemView.findViewById(R.id.recTxtVisitsDate);
            visitsSentence = itemView.findViewById(R.id.recTxtVisitsSentence);
            layVisitsItem = itemView.findViewById(R.id.recLayVisits);
        }


    }


}
