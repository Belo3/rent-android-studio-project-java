package com.example.inz.adapterRecacleView;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.BreakDownID;
import com.example.inz.Data.MySingleton;
import com.example.inz.Data.VisitsID;
import com.example.inz.InsertView.InsertPayment;
import com.example.inz.R;

public class BreakdownAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {





    private FetchJsonClass fetchJsonClass = new FetchJsonClass();
    private BreakDownID breakDownID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;
    private RecyclerView recyclerView;

    public BreakdownAdapter(Context mContext, BreakDownID breakDownID, FragmentManager fragmentManager,RecyclerView recyclerView) {
        this.breakDownID= breakDownID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemViewType(int position) {
        if(Markers.ifLessorLessee==0) {
            return 0;
        } else {
            return 1;
        }
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if(viewType != 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_breakdown, parent, false);
            return new BreakdownAdapter.BreakdownHolder (view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_breakdown_lessee, parent, false);
            return new BreakdownAdapter.BreakdownLesseeHolder (view);
        }
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if(Markers.ifLessorLessee == 0 ) {
            populateBreakdownViewHolder(holder, position);
        } else {
            populateBreakdownLesseeViewHolder(holder, position);
        }



    }


    @Override
    public int getItemCount() {
        return breakDownID.getBreakDownID().size();
    }


    class BreakdownHolder extends RecyclerView.ViewHolder {

        TextView breakdownName, breakdownDate, breakdownSentence;
        Button btnBreakdown;
        RelativeLayout layBreakdownItem;

        BreakdownHolder(View itemView) {
            super(itemView);
            breakdownName = itemView.findViewById(R.id.recTxtBreakdownName);
            breakdownDate = itemView.findViewById(R.id.recTxtBreakdownDate);
            breakdownSentence = itemView.findViewById(R.id.recTxtBreakdownSentence);
            btnBreakdown = itemView.findViewById(R.id.recBtnBreakdownAccept);
            layBreakdownItem = itemView.findViewById(R.id.recLayBreakdown);
        }




    }
    class BreakdownLesseeHolder extends RecyclerView.ViewHolder {

        TextView breakdownName, breakdownDate, breakdownSentence, btnBreakdown;
        RelativeLayout layBreakdownItem;

        BreakdownLesseeHolder(View itemView) {
            super(itemView);
            breakdownName = itemView.findViewById(R.id.recTxtBreakdownName);
            breakdownDate = itemView.findViewById(R.id.recTxtBreakdownDate);
            breakdownSentence = itemView.findViewById(R.id.recTxtBreakdownSentence);
            btnBreakdown = itemView.findViewById(R.id.recBtnBreakdownAccept);
            layBreakdownItem = itemView.findViewById(R.id.recLayBreakdownLessee);
        }
    }


        private void populateBreakdownViewHolder(final RecyclerView.ViewHolder holder,final int position) {

            ((BreakdownHolder) holder).breakdownName.setText(breakDownID.getBreakDownID().get(position).getBreakdownName());
            ((BreakdownHolder) holder).breakdownDate.setText(breakDownID.getBreakDownID().get(position).getDateAdded());
            ((BreakdownHolder) holder).breakdownSentence.setText(breakDownID.getBreakDownID().get(position).getBreakdownSentence());

            if (breakDownID.getBreakDownID().get(position).getBreakdownAccepted() == 1) {
                ((BreakdownHolder) holder).btnBreakdown.setText("Resolve");
            }


            ((BreakdownHolder) holder).btnBreakdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Activity activity = (Activity) mContext;

                    if (breakDownID.getBreakDownID().get(position).getBreakdownAccepted() == 0) {
                        MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlAcceptBreakdown(mContext,fragmentManager,breakDownID.getBreakDownID().get(position).getBreakdownID(),0, recyclerView));
                        MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.getHousesJson(mContext,fragmentManager));
                    }

                    else{
                        MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.jsonCallUrlAcceptBreakdown(mContext,fragmentManager,breakDownID.getBreakDownID().get(position).getBreakdownID(),1, recyclerView));
                        MySingleton.getInstance(mContext).addToRequestQueue(fetchJsonClass.getHousesJson(mContext,fragmentManager));
                    }




                }
            });

        }


    private void populateBreakdownLesseeViewHolder(final RecyclerView.ViewHolder holder,final int position) {

        ((BreakdownLesseeHolder) holder).breakdownName.setText(breakDownID.getBreakDownID().get(position).getBreakdownName());
        ((BreakdownLesseeHolder) holder).breakdownDate.setText(breakDownID.getBreakDownID().get(position).getDateAdded());
        ((BreakdownLesseeHolder) holder).breakdownSentence.setText(breakDownID.getBreakDownID().get(position).getBreakdownSentence());

        if (breakDownID.getBreakDownID().get(position).getBreakdownAccepted() == 1) {
            ((BreakdownLesseeHolder) holder).btnBreakdown.setText("Accepted");
        }  else{
            ((BreakdownLesseeHolder) holder).btnBreakdown.setText("Not Accepted");
        }

    }

}
