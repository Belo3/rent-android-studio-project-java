package com.example.inz.adapterRecacleView;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.AgreementID;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.Message;
import com.example.inz.ViewFromFragments.Payment;

public class AgreementsAdapter extends RecyclerView.Adapter<AgreementsAdapter.AgreementsViewsHolder> {

    private AgreementID agreementID;
    private Context mContext;
    private android.support.v4.app.FragmentManager fragmentManager;


    public AgreementsAdapter(Context mContext, AgreementID agreementID, FragmentManager fragmentManager) {
        this.agreementID = agreementID;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public AgreementsViewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_house_agreement, parent, false);
        return new AgreementsViewsHolder(view);
    }


    @Override
    public void onBindViewHolder(final AgreementsViewsHolder holder, final int position) {

        holder.agreementName.setText(agreementID.getAgreementID().get(position).getAgreementName());
        holder.agreementBalance.setText(Integer.toString(agreementID.getAgreementID().get(position).getBalance())+" $");
        holder.agreementDateFrom.setText(agreementID.getAgreementID().get(position).getDateFrom());
        holder.agreementDateTo.setText(agreementID.getAgreementID().get(position).getDateTo());
        if (agreementID.getAgreementID().get(position).getNewMessOwner() !=0 ){
            holder.messages.setText("Messages " + Integer.toString(agreementID.getAgreementID().get(position).getNewMessOwner()));
            holder.messages.setTextColor(mContext.getColor(R.color.green));
        }

        if (agreementID.getAgreementID().get(position).getBalance() < 0) {
            holder.agreementBalance.setTextColor(ContextCompat.getColor(mContext, R.color.red));

        }
        holder.messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Markers.positionAgreement = position;
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Message()).addToBackStack(null).commit();

            }
        });

        holder.payments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Markers.positionAgreement = position;
                fragmentManager.beginTransaction().replace(R.id.content_frame, new Payment()).addToBackStack(null).commit();
            }
        });

        holder.layAgreementItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Markers.positionHouse = position;
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new HousesDetail()).addToBackStack(null).commit();
            }
        });

    }


    @Override
    public int getItemCount() {
        return agreementID.getAgreementID().size();
    }


    class AgreementsViewsHolder extends RecyclerView.ViewHolder {

        TextView agreementName, agreementBalance, agreementDateFrom, agreementDateTo;
        Button payments, messages;

        RelativeLayout layAgreementItem;

        AgreementsViewsHolder(View itemView) {
            super(itemView);
            agreementBalance = itemView.findViewById(R.id.recAgreBalance);
            agreementName = itemView.findViewById(R.id.recAgreName);
            payments = itemView.findViewById(R.id.recAgrePayment);
            messages=  itemView.findViewById(R.id.recAgreMessage);
            agreementDateFrom = itemView.findViewById(R.id.recAgreDateFrom);
            agreementDateTo = itemView.findViewById(R.id.recAgreDateTo);
            layAgreementItem = itemView.findViewById(R.id.recAgreItem);
        }


    }
}
