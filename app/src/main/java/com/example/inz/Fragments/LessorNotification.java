package com.example.inz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.google.gson.Gson;

public class LessorNotification extends Fragment {

    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    RelativeLayout relativeLayoutNotification;
    RecyclerView recyclerViewNotificationDamage ;
    LinearLayoutManager linearLayoutManagerNotificationAgreement;
    SharedPreferences sharedPref;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();
    LinearLayoutManager linearLayoutManagerDamage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewNotifi= inflater.inflate(R.layout.notification_lessor, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Owner Notification");

        recyclerViewNotificationDamage = viewNotifi.findViewById(R.id.recViewNotificationsDamageLessor);

        linearLayoutManagerDamage = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);


        recyclerViewNotificationDamage.setLayoutManager(linearLayoutManagerDamage);
        relativeLayoutNotification = viewNotifi.findViewById(R.id.layNotificationsLessor);

        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAdapterDamageOwner(getContext(),getFragmentManager(),recyclerViewNotificationDamage));

        return viewNotifi;
    }
}
