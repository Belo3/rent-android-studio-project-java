package com.example.inz.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.google.gson.Gson;

public class UserInfo extends Fragment {
    CheckBox checkLessor, checkLessee;
    EditText editUserName, editUserSurName, editUserEmail,  editUserNumber;
    String stringName, stringSurName, stringNumber;
    String stringName2, stringSurName2, stringNumber2;
    Button btnUpdateInfo;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();
    RelativeLayout relativeLayout;
    Gson gson = new Gson();
    LoginID loginid;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewUserInfo = inflater.inflate(R.layout.user_info,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("User Info");

        editUserName = viewUserInfo.findViewById(R.id.eTxtName);
        editUserSurName = viewUserInfo.findViewById(R.id.eTxtSurName);
        editUserNumber = viewUserInfo.findViewById(R.id.eTxtNumber);
        btnUpdateInfo = viewUserInfo.findViewById(R.id.btnRegistration);
        checkLessee = viewUserInfo.findViewById(R.id.checkLessee);
        checkLessor= viewUserInfo.findViewById(R.id.checkLessor);
        relativeLayout = viewUserInfo.findViewById(R.id.layUserInfo);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.userName,"error");


        loginid = gson.fromJson(jsonData , LoginID.class);
        stringName = loginid.getLoginData().get(0).getName();
        stringSurName = loginid.getLoginData().get(0).getSurName();
        stringNumber = loginid.getLoginData().get(0).getNumber();
        Integer owner = loginid.getLoginData().get(0).getOwner();
        Integer rentier = loginid.getLoginData().get(0).getRentier();


        editUserName.setText(stringName);
        editUserSurName.setText(stringSurName);
        editUserNumber.setText(stringNumber);
        if(owner==1) {
            checkLessor.setChecked(true);
            checkLessor.setEnabled(false);
        }

        if(rentier==1) {
            checkLessee.setChecked(true);
            checkLessee.setEnabled(false);
        }

        btnUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stringName = editUserName.getText().toString();
                stringSurName = editUserSurName.getText().toString();
                stringNumber = editUserNumber.getText().toString();


                stringName2 = loginid.getLoginData().get(0).getName();
                stringSurName2 = loginid.getLoginData().get(0).getSurName();
                stringNumber2 = loginid.getLoginData().get(0).getNumber();
                if (stringName.equals(stringName2) & stringSurName.equals(stringSurName2) & stringNumber.equals(stringNumber2))
                {
                    CharSequence text = "Change any info to update!";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();

                }
                else{
                    MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlUpdateUserInfo(getContext(),getFragmentManager(),stringName,stringSurName,stringNumber,getView()));
                }
            }
        });
        return viewUserInfo;
    }
}
