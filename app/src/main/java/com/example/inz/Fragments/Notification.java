package com.example.inz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.google.gson.Gson;

public class Notification extends Fragment {
    JsonObjectRequest jsonRequest;
    Gson gson = new Gson();
    LinearLayout relativeLayoutNotification;
    RecyclerView recyclerViewNotificationAgreement, recyclerViewNotificationVisits, recyclerViewNotificationDamage ;
    LinearLayoutManager linearLayoutManagerNotificationAgreement;
    SharedPreferences sharedPref;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();
    LinearLayoutManager linearLayoutManagerNotification, linearLayoutManagerDamage, linearLayoutManagerVisits;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewNotification= inflater.inflate(R.layout.notifications_lesse, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Notification Lessee");
        recyclerViewNotificationAgreement = viewNotification.findViewById(R.id.recViewNotifications);
        recyclerViewNotificationVisits = viewNotification.findViewById(R.id.recViewNotificationsVisits);
        recyclerViewNotificationDamage = viewNotification.findViewById(R.id.recViewNotificationsDamage);

        linearLayoutManagerNotification = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        linearLayoutManagerDamage = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        linearLayoutManagerVisits = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerViewNotificationAgreement.setLayoutManager(linearLayoutManagerNotification);
        recyclerViewNotificationVisits.setLayoutManager(linearLayoutManagerVisits);
        recyclerViewNotificationDamage.setLayoutManager(linearLayoutManagerDamage);
        relativeLayoutNotification = viewNotification.findViewById(R.id.layNotifications);
        Log.i("im here","Notify");
        recyclerViewNotificationDamage.setNestedScrollingEnabled(false);


        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAdapterNotifyAgreement(getContext(),getFragmentManager(),recyclerViewNotificationAgreement));
        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAdapterNotifyDamage(getContext(),getFragmentManager(),recyclerViewNotificationDamage));
        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlAdapterNotifyVisits(getContext(),getFragmentManager(),recyclerViewNotificationVisits));
        return viewNotification;
    }
}
