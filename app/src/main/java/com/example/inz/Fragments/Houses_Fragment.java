package com.example.inz.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.InsertView.InsertHouse;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.ViewFromFragments.HousesDetail;
import com.example.inz.adapterRecacleView.HousesAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import static android.view.View.generateViewId;

public class Houses_Fragment extends Fragment {

    JsonObjectRequest jsonRequest;
    SharedPreferences sharedPref;
    Gson gson = new Gson();
    TextView test;
    Button btnHouse;
    RelativeLayout layoutHouses;
    RecyclerView recViewHouses;
    HousesAdapter housesAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    FloatingActionButton btnAddHouse;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View ViewHouse = inflater.inflate(R.layout.houses,container,false);
        Markers.ifLessorLessee = 0;

        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Houses");
        recViewHouses = ViewHouse.findViewById(R.id.recViewHouses);

        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled},
                new int[] { android.R.attr.state_pressed}
        };
        int[] colors = new int[] {
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blackerBlue)
        };
        ColorStateList fabColorList = new ColorStateList(states, colors);
        ViewHouse.findViewById(R.id.flotingBtnAddHouse).setBackgroundTintList(fabColorList);


        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recViewHouses.setLayoutManager(mLinearLayoutManager);


        test = ViewHouse.findViewById(R.id.txtHouse);
        layoutHouses = ViewHouse.findViewById(R.id.layHouses);
        btnAddHouse = ViewHouse.findViewById(R.id.flotingBtnAddHouse);

        btnAddHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager().beginTransaction().replace(R.id.content_frame,new InsertHouse()).addToBackStack(null).commit();
            }
        });


        MySingleton.getInstance(getContext()).addToRequestQueue(getHousesJson());




        return ViewHouse;
    }



//










    private JsonObjectRequest getHousesJson() {
        String url = "https://aligator1.herokuapp.com/select/house";


        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.userName,"error");
        final SharedPreferences.Editor editor = sharedPref.edit();


        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                HouseID houseID = gson.fromJson(jsonData.toString() , HouseID.class);
                editor.putString("houses", jsonData.toString());
                editor.apply();

                try {
                    houseID.getHouseID().get(0).getHouseName();
                    layoutHouses.removeView(test);
                    housesAdapter = new HousesAdapter(getActivity(),houseID, getFragmentManager());
                    recViewHouses.setAdapter(housesAdapter);


                    }
                catch (Exception e) {
                    e.printStackTrace();
                    test.setText("No Houses :( ");
                    test.setTextSize(40);
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                test.setText("Rest in rib");
            }
        });


        return jsonRequest;
    }
}
