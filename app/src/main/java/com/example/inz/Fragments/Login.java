package com.example.inz.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.InsertView.InsertHouse;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.ViewFromFragmentsLesse.HouseAgreementView;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends Fragment {
    JsonObjectRequest jsonRequest;
    SharedPreferences sharedPref;
    Gson gson = new Gson();
    TextView TestLogin;
    Button Login;
    EditText Email, Password;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View ViewLogin= inflater.inflate(R.layout.login, container, false);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Login");
        TestLogin = ViewLogin.findViewById(R.id.txtLogin);
        Login = ViewLogin.findViewById(R.id.btnLogin);
        Email = ViewLogin.findViewById(R.id.eTxtEmail);
        Password = ViewLogin.findViewById(R.id.eTxtPass);

        Log.i("login", "LOGINNNNN");

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(Email.getText() == null || Password.getText()==null)) {
                    MySingleton.getInstance(getContext()).addToRequestQueue(getJsonRequest(Email.getText().toString(),Password.getText().toString()));
                    hideKeyboardFrom(getContext(),getView());
                }
            }
        });




        try{
            sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            String jsonData = sharedPref.getString(Markers.userName,"error");
            final SharedPreferences.Editor editor = sharedPref.edit();
            LoginID loginid = gson.fromJson(jsonData , LoginID.class);
            Log.i("lool",Integer.toString(loginid.getLoginData().get(0).getOwner()));

            if(loginid.getLoginData().get(0).getOwner() == 1){

                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Houses_Fragment()).commit();

            }else {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new RentierHousesFragment()).commit();
            }
        }
        catch (Exception i){

        }





        return ViewLogin;
    }




    public JsonObjectRequest getJsonRequest(String Email, String Password) {
        String url = "https://aligator1.herokuapp.com/select/login";

        Map<String, String> params = new HashMap();
        params.put("email",Email);
        params.put("haslo",Password);
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                LoginID loginid = gson.fromJson(jsonData.toString() , LoginID.class);

                String omega = loginid.getLoginData().get(0).getEmail();

                // String house = houseID.getHouseID().get(0).getIdHouse();

                TestLogin.setText("Response: "+ omega);


                sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(Markers.userName, jsonData.toString());
                editor.apply();
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new Houses_Fragment()).commit();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                TestLogin.setText("Wrong Credentials");
            }
        });


        return jsonRequest;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
