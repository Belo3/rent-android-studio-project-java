package com.example.inz.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.inz.ControlerVariable.FetchJsonClass;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.google.gson.Gson;

public class Registration extends Fragment {
    CheckBox checkLessor, checkLessee;
    EditText editUserName, edidUserSurName, editUserEmail, editUserPassword, editUserNumber, editSecondPassword;
    String stringName, stringSurName, stringEmail, stringPassword, stringNumber, stringSecondPassword;
    Button btnAddAgreement;
    FetchJsonClass fetchJsonClass = new FetchJsonClass();
    RelativeLayout relativeLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewRegistration = inflater.inflate(R.layout.registration,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Registration");


        editUserName = viewRegistration.findViewById(R.id.eTxtName);
        edidUserSurName = viewRegistration.findViewById(R.id.eTxtSurName);
        editUserEmail = viewRegistration.findViewById(R.id.eTxtEmail);
        editUserPassword = viewRegistration.findViewById(R.id.eTxtPass);
        editUserNumber = viewRegistration.findViewById(R.id.eTxtNumber);
        editSecondPassword = viewRegistration.findViewById(R.id.eTxtSecondPassword);
        btnAddAgreement = viewRegistration.findViewById(R.id.btnRegistration);
        checkLessee = viewRegistration.findViewById(R.id.checkLessee);
        checkLessor= viewRegistration.findViewById(R.id.checkLessor);
        relativeLayout = viewRegistration.findViewById(R.id.layRegistration);
            btnAddAgreement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stringName = editUserName.getText().toString();
                    stringSurName = edidUserSurName.getText().toString().trim();
                    stringEmail = editUserEmail.getText().toString().trim();
                    stringPassword = editUserPassword.getText().toString().trim();
                    stringNumber = editUserNumber.getText().toString().trim();
                    stringSecondPassword = editSecondPassword.getText().toString().trim();

                    if((stringName.isEmpty()) | (stringSurName.isEmpty()) | stringEmail.isEmpty() | stringPassword.isEmpty() | stringNumber.isEmpty() | stringPassword.isEmpty() ){
                        CharSequence text = "Fill All";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(getContext(), text, duration);
                        toast.show();
                    }
                    else if (!(stringPassword.equals(stringSecondPassword))){
                        CharSequence text = "Wrong Repeat Password";
                        int duration = Toast.LENGTH_SHORT;
                        editSecondPassword.setBackgroundColor(getResources().getColor(R.color.red));
                        Toast toast = Toast.makeText(getContext(), text, duration);
                        toast.show();
                    }
                    else if (checkLessee.isChecked() | checkLessor.isChecked()){

                        MySingleton.getInstance(getContext()).addToRequestQueue(fetchJsonClass.jsonCallUrlRegister(getContext(),
                                getFragmentManager(),stringEmail,stringPassword,stringName,stringSurName,stringNumber,checkLessor.isChecked(),checkLessee.isChecked(),viewRegistration));



                    }
                }
            });












        return viewRegistration;
    }



}
