package com.example.inz.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.inz.ControlerVariable.Markers;
import com.example.inz.Data.HouseID;
import com.example.inz.Data.LoginID;
import com.example.inz.Data.MySingleton;
import com.example.inz.MainActivity;
import com.example.inz.R;
import com.example.inz.adapterRecacleView.HousesLesseAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RentierHousesFragment extends Fragment {
    RelativeLayout relativeLayout;
    FloatingActionButton floatingActionButton;
    SharedPreferences sharedPref;
    private LinearLayoutManager mLinearLayoutManager;
    Gson gson = new Gson();
    TextView test;
    JsonObjectRequest jsonRequest;
    HousesLesseAdapter housesAdapter;
    RecyclerView recyclerViewHousesLesse;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View viewHousesLessee = inflater.inflate(R.layout.houses,container,false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Houses Lessee");
        Markers.ifLessorLessee = 1;
        relativeLayout = viewHousesLessee.findViewById(R.id.layHouses);
        floatingActionButton = viewHousesLessee.findViewById(R.id.flotingBtnAddHouse);
        relativeLayout.removeView(floatingActionButton);
        test = viewHousesLessee.findViewById(R.id.txtHouse);
        recyclerViewHousesLesse = viewHousesLessee.findViewById(R.id.recViewHouses);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerViewHousesLesse.setLayoutManager(mLinearLayoutManager);

        MySingleton.getInstance(getContext()).addToRequestQueue(getHousesJson());


        return  viewHousesLessee;
    }






    private JsonObjectRequest getHousesJson() {
        String url = "https://aligator1.herokuapp.com/select/houseAgrementForRent";


        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String jsonData = sharedPref.getString(Markers.userName,"error");
        final SharedPreferences.Editor editor = sharedPref.edit();


        LoginID loginid = gson.fromJson(jsonData , LoginID.class);
        String userID = loginid.getLoginData().get(0).getIdUser();

        Map<String, String> params = new HashMap();
        params.put("userID",userID);
        JSONObject parameters = new JSONObject(params);

        jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonData) {
                HouseID houseID = gson.fromJson(jsonData.toString() , HouseID.class);
                editor.putString(Markers.housesLesseeName, jsonData.toString());
                editor.apply();

                try {
                    houseID.getHouseID().get(0).getHouseName();
                    relativeLayout.removeView(test);
                    housesAdapter = new HousesLesseAdapter(getActivity(),houseID, getFragmentManager());
                    recyclerViewHousesLesse.setAdapter(housesAdapter);


                }
                catch (Exception e) {
                    e.printStackTrace();
                    test.setText("No Houses with active agreement :( ");
                    test.setTextSize(40);
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                test.setText("Error");
            }
        });


        return jsonRequest;
    }
}
